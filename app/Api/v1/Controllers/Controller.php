<?php

namespace App\Api\v1\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function accesstoken($id,$access_token)
    {
      $builds = DB::table('users')->where('id', $id)->where('access_token', $access_token)->get();
      // print_r($builds);die;
      if(count($builds)==1)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
}
