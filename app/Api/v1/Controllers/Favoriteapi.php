<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Favorite;
use App\Gallery;
use App\PostCategory;

class Favoriteapi extends Controller
{
  public function store(Request $request)
  {
      $favorite = new Favorite();
      $id = $request->input("user_id");
      $access_token = $request->input("access_token");
      $auth=$this->accesstoken($id,$access_token);
      if(!empty($request->input('access_token')) && !empty($request->input('user_id'))){
        if($auth==1){
          $gallery = Gallery::find($request->input("gallery_id"));
          if(!empty($gallery)){
            $exsist = Favorite::where('gallery_id', $request->input("gallery_id"))
            ->where('user_id', $request->input('user_id'))->get();
            if(count($exsist) == 0){
              $favorite->user_id = $request->input("user_id");
              $favorite->gallery_id = $request->input("gallery_id");
              $favorite->sub_category_id = $request->input("sub_category_id");


              if($favorite->save()){
                $result['message'] = 'favorite';
                $success = 'true';
                return compact('success','result');
              }else{
                $success = 'false';
                $result['message'] = 'not save';
                return compact('success', 'result');
              }
            }else{
               if($exsist[0]->delete()){
                 $result['message'] = 'un-favorite';
                 $success = 'true';
                 return compact('success','result');
               }else{
                 $result['message'] = 'unsuccessful';
                 $success = 'false';
                 return compact('success','result');
               }
            }
          }else{
            $result['message'] = 'invalid gallery';
            $success = 'false';
            return compact('success','result');
          }

        }else{
          $result['message'] = 'User Not Access';
          $success = 'false';
          return compact('success','result');
        }
    }else{
      $result['message'] = 'invalid token';
      $success = 'false';
      return compact('success','result');
    }
  }





  public function user_favorite(Request $request){
    $post = array();
    $img = array();

    $id = $request->input("user_id");
    $access_token = $request->input("access_token");
    if(!empty($request->input('access_token')) && !empty($request->input('user_id'))){
      $auth=$this->accesstoken($id,$access_token);
      // print_r($auth);die;
      if($auth==1){
        $user_id = $request->input('user_id');
        $image_id = Favorite::where('user_id', $user_id)->get();
        if(!empty($image_id)){
          foreach ($image_id as $key => $value) {
            $img[$key]['gallery_id'] = $value->gallery_id;
            $img[$key]['sub_category_id'] = $value->sub_category_id;
          }
          if(!empty($img)){
            foreach ($img as $key => $i) {
              // return compact('i');die;
              $galleries[$key] = Gallery::find($i['gallery_id']);
              $galleries[$key]['sub_category_id'] = $i['sub_category_id'];
              // $galleries[$key]->image_url = asset('/uploads').'/'.$galleries[$key]->image_name;
              if(empty($galleries[$key])){
                unset($galleries[$key]);
              }
            }
            // return compact('galleries');die;

            foreach ($galleries as $key => $value) {
              $post[$key]['id'] = $value->id;
              $post[$key]['title'] = $value->title;
              $post[$key]['object_name'] = $value->object_name;
              $post[$key]['materials'] = $value->materials;
              $post[$key]['description'] = $value->description;
              $post[$key]['overall'] = $value->overall;
              $post[$key]['date'] = $value->date;
              $post[$key]['history'] = $value->history;
              $post[$key]['credit'] = $value->credit;
              $post[$key]['dimensions'] = $value->dimensions;
              $post[$key]['accession'] = $value->accession;
              $post[$key]['image_name'] = $value->image_name;
              $post[$key]['image_size'] = $value->image_size;
              $post[$key]['image_url'] = asset('/uploads/galleries').'/'.$value->image_name;
              $post[$key]['favorite'] = "1";
              $post[$key]['sub_category_id'] = $value->sub_category_id;
              $postcat_ids = explode(',', $value->post_category_id);
              foreach ($postcat_ids as $key1 => $value1) {
                $result1[$key1] =  $this->gallery_by_id($postcat_ids[$key1]);
                if(empty($result1[$key1])){
                  unset($result1[$key1]);
                }
              }

              $cat ="";
              $cul = "";
              $art ="";
              foreach ($result1 as $k => $v) {
                  if($v->main_categories_id == "1"){
                    $art = $v->name;
                  }
                  elseif ($v->main_categories_id == "2") {
                    $cat  .= $v->name.',';
                  }
                  elseif ($v->main_categories_id == "3") {
                    $cul .= $v->name.',';
                  }
              }
                $post[$key]['artist'] = $art;
                $post[$key]['category'] = $cat;
                $post[$key]['culture'] = $cul;
                $galleries = array();
                $galleries[$key] = $post[$key];
            }

            $result['galleries'] = $post;

            $success = 'true';
            return compact( 'result','success');
          }else{
            $result['message'] = 'no data found';
            $success = 'false';
            return compact( 'result', 'success');
          }
        }else{
          $success = 'false';
          $result['message'] = 'no data found';
          return compact('success','result');
        }
    }else{
      $result['message'] = 'User Not Access';
      $success = 'false';
      return compact('success','result');
    }
  }else {
    $success = 'false';
    $result['message'] = 'invalid token';
    return compact('success','result');
   }
  }


  public function gallery_by_id($id){
    return $post =  PostCategory::find($id);
  }


}
