<?php

// namespace App\Http\Controllers;
namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\MainCategory;
use App\PostCategory;
use DB;
use App\Http\Requests;
use App\Gallery;
use App\GalleriesPostCategory;
use App\Favorite;

class Galleryapi extends Controller
{
  public function index(Request $request)
  {
      $galleries = Gallery::all();
    if(!empty($galleries)){

     foreach ($galleries as $key => $value) {
       $galleries[$key]->image_url = asset('/uploads').'/'.$value->image_name;
        $main_cat = MainCategory::find($value->main_categories_id);
        $sub_cat = PostCategory::find($value->post_categories_id);
        $galleries[$key]->main_categories_name = $main_cat->title;
        $galleries[$key]->sub_category_name = $sub_cat->name;
     }
     $result = 'success';
     // returncompact('galleries', 'result');
   }else{
     $error = 'no data';
     // returncompact('error');
   }
  }

  public function show($id)
  {
    $gallery = Gallery::find($id);
    if(!empty($gallery)){
      $main_cat = MainCategory::find($gallery->main_categories_id);
      $sub_cat = PostCategory::find($gallery->post_categories_id);
      $gallery->main_categories_name = $main_cat->title;
      $gallery->sub_category_name = $sub_cat->name;
      $gallery->image_url = asset('/uploads').'/'.$gallery->image_name;
      $result = 'success';
      // returncompact('gallery', 'result');
    }else{
      $error = 'no data';
      // returncompact('error');
    }
  }

  // gallery by sub category id
  // public function gallery_by_subcategory(Request $request){
  //   $sub_category_id = $request->input('id');
  //   if(!empty($sub_category_id)){
  //
  //   $galleries = Gallery::where('post_categories_id', $sub_category_id)->get();
  //   foreach ($galleries as $key => $value) {
  //     $galleries[$key]->image_url = asset('/uploads').'/'.$value->image_name;
  //      $main_cat = MainCategory::find($value->main_categories_id);
  //      $sub_cat = PostCategory::find($value->post_categories_id);
  //      $galleries[$key]->main_categories_name = $main_cat->title;
  //      $galleries[$key]->sub_category_name = $sub_cat->name;
  //   }
  //   $result = 'success';
  //   // returncompact('galleries', 'result');
  // }else{
  //   $error = 'invalid id';
  //   // returncompact('error');
  // }
  // }


  public function gallery_search(Request $request)
  {
    $post = array();
    $posts = array();

    $img = array();

    $title = $request->input("title");
    $object_name = $request->input("object_name");
    $category_id = $request->input("category_id");
    $culture_id = $request->input("culture_id");
    $artist_id = $request->input("artist_id");
    $user_id = $request->input("user_id");
    $access_token = $request->input("access_token");


    $date = $request->input("date");
    $accession = $request->input("accession");
    $materials = $request->input("materials");
    $main_categories_id = $request->input("main_categories_id");
    // DB::enableQueryLog();


    if (!empty($artist_id) or !empty($culture_id) or !empty($category_id)){

      $query = DB::table('galleries')
      ->join('galleries_post_category', function ($join) use($title, $object_name, $category_id,$culture_id, $artist_id, $date, $accession, $materials) {
        $join->on('galleries.id', '=', 'galleries_post_category.gallery_id')
        ->where('galleries.object_name', 'LIKE', '%'.$object_name.'%')
        ->orWhere('galleries.date', 'LIKE', '%'.$date.'%')
        ->orWhere('galleries.accession', 'LIKE', '%'.$accession.'%')
        ->orWhere('galleries.materials', 'LIKE', '%'.$materials.'%');

      });

      if(!empty($artist_id)){
        $query->orWhere('galleries_post_category.post_categories_id', '=', $artist_id);
      }
      if(!empty($category_id)){
        $query->orWhere('galleries_post_category.post_categories_id', '=', $category_id);
      }
      if(!empty($culture_id)){
        $query->orWhere('galleries_post_category.post_categories_id', '=', $culture_id);
      }


      $galleries = $query->groupBy('post_category_id')->get();
      // return compact('galleries');die;
      $new_gallery = array();

      foreach ($galleries as $key => $value) {
        $post_category = explode(',', $value->post_category_id);
        if(in_array($artist_id, $post_category) and in_array($culture_id, $post_category) and in_array($category_id, $post_category)){
          $new_gallery[]  = $galleries[$key];
        }
      }

      foreach ($new_gallery as $key => $value) {
        $postcat_ids = explode(',', $value->post_category_id);
        foreach ($postcat_ids as $key1 => $value1) {
          $result= "";
          $result['cat'] =  $this->find_post_category($postcat_ids[$key1]);
          $result['gallery'] = $value->gallery_id;
          array_push($posts, $result);
        }
      }


      $post2 = array();
      foreach ($new_gallery as $key => $value) {
        foreach ($posts as $key2 => $value2) {
         if($value->gallery_id == $value2['gallery'])
         {
           $post2[$key]['galleries'] = $value;
           $post2[$key][$key2] = $value2;
           $ab = $value2['cat'];
           if($ab->main_categories_id == 1){
             $post2[$key]['galleries']->artist_id = $ab->name;
           }
           elseif ($ab->main_categories_id == 2) {
             $post2[$key]['galleries']->category_id .= $ab->name.',';
           }
           elseif ($ab->main_categories_id == 3) {
             $post2[$key]['galleries']->culture_id .= $ab->name.',';
           }
         }
        }
      }


      $result = array();
      $galleries1 = array();
      foreach ($post2 as $key => $value) {
        $galleries1[$key] = $value['galleries'];
        $galleries1[$key]->image_url = asset('/uploads/galleries').'/'.$value['galleries']->image_name;
        $galleries1[$key]->sub_category_id = $value['galleries']->post_categories_id;
        $galleries1[$key]->id = $value['galleries']->gallery_id;
        if(!empty($value['galleries']->artist_id)){
          $galleries1[$key]->artist = $value['galleries']->artist_id;
        }else {
          $galleries1[$key]->artist = "";
        }
        if(!empty($value['galleries']->category_id)){
          $galleries1[$key]->category = $value['galleries']->category_id;
        }else {
          $galleries1[$key]->category = "";
        }

        if(!empty($value['galleries']->culture_id)){
          $galleries1[$key]->culture = $value['galleries']->culture_id;
        }else{
          $galleries1[$key]->culture = "";
        }

        if(!empty($user_id) and !empty($access_token)){
          $fav = Favorite::where('user_id', $user_id)->where('gallery_id', $value['galleries']->id)->get();
          if(count($fav) == 1){
            $galleries1[$key]->favorite = '1';
          }else{
            $galleries1[$key]->favorite = '0';
          }
        }else{
           $galleries1[$key]->favorite = '0';
       }

 


        unset($galleries1[$key]->main_categories_id);
        unset($galleries1[$key]->post_category_id);
        unset($galleries1[$key]->created_at);
        unset($galleries1[$key]->updated_at);
        unset($galleries1[$key]->post_categories_id);
        unset($galleries1[$key]->gallery_id);

        unset($galleries1[$key]->artist_id);
        unset($galleries1[$key]->category_id);
        unset($galleries1[$key]->culture_id);
        unset($galleries1[$key]->image_type);
      }

        $result['galleries']  = $galleries1;
$success = "true";
      return compact('result', 'success');die;

    }else{
      $galleries = array();

      $query = DB::table('galleries');
      if(!empty($title)){
        $query->where('title', 'LIKE', '%'.$title.'%');
      }
      if(!empty($object_name)){
        $query->orWhere('object_name', 'LIKE', '%'.$object_name.'%');
      }
      if(!empty($date)){
        $query->orWhere('date', 'LIKE', '%'.$date.'%');
      }
      if(!empty($accession)){
        $query->orWhere('accession', 'LIKE', '%'.$accession.'%');
      }
      if(!empty($materials)){
          $query->orWhere('materials', 'LIKE', '%'.$materials.'%');
      }
      $galleries = $query->get();

      // return compact('galleries');die;

      $post = array();
      $post22 = array();


      foreach ($galleries as $key => $value) {
        $post[$key]['id'] = $value->id;
        $post[$key]['title'] = $value->title;
        $post[$key]['object_name'] = $value->object_name;
        $post[$key]['materials'] = $value->materials;
        $post[$key]['description'] = $value->description;
        $post[$key]['overall'] = $value->overall;
        $post[$key]['date'] = $value->date;
        $post[$key]['history'] = $value->history;
        $post[$key]['credit'] = $value->credit;
        $post[$key]['dimensions'] = $value->dimensions;
        $post[$key]['accession'] = $value->accession;
        $post[$key]['image_name'] = $value->image_name;
        $post[$key]['image_size'] = $value->image_size;
        $post[$key]['image_url'] = asset('/uploads/galleries').'/'.$value->image_name;


       if(!empty($user_id) and !empty($access_token)){
          $fav = Favorite::where('user_id', $user_id)->where('gallery_id', $post[$key]['id'])->get();
          if(count($fav) == 1){
            $post[$key]['favorite'] = '1';
          }else{
            $post[$key]['favorite']= '0';
          }
        }else{
           $post[$key]['favorite'] = '0';
        }


        // $post[$key]['favorite'] = "1";
        $postcat_ids = explode(',', $value->post_category_id);
        foreach ($postcat_ids as $key1 => $value1) {
          $result1[$key1] =  $this->gallery_by_id($postcat_ids[$key1]);
          if(empty($result1[$key1])){
            unset($result1[$key1]);
          }
        }

        // return compact('post');die;

        $cat ="";
        $cul = "";
        $art ="";
        foreach ($result1 as $k => $v) {
            if($v->main_categories_id == "1"){
              $art = $v->name;
            }
            elseif ($v->main_categories_id == "2") {
              $cat  .= $v->name.',';
            }
            elseif ($v->main_categories_id == "3") {
              $cul .= $v->name.',';
            }
        }
          $post[$key]['artist'] = $art;
          $post[$key]['category'] = $cat;
          $post[$key]['culture'] = $cul;


 

          $galleries = array();
          $galleries[$key] = $post[$key];

         $post22[$key] = $post[$key];

      }

      $result['galleries']  = $post22;
      $success = "true";
      return compact('result', 'success');die;

    }

  }


  public function find_post_category($post_category_id){
    $post = PostCategory::find($post_category_id);
    return $post;
  }



  public function all_subcategory()
  {
    $artist = PostCategory::where('main_categories_id', 1)->get();
    $category = PostCategory::where('main_categories_id', 2)->get();
    $culture = PostCategory::where('main_categories_id', 3)->get();
    $all_cat['artist'] = $artist;
    $all_cat['category'] = $category;
    $all_cat['culture'] = $culture;
    $result['sub_category'] = $all_cat;
    $success = "true";
    return compact('result', 'success');
  }




  //
  // public function by_artist(Request $request){
  //   if(!empty($request->input("artist_id"))){
  //     $artist_id = $request->input("artist_id");
  //     $gallery = Gallery::where('artist_id', $artist_id)->get();
  //     $result = "success";
  //     // returncompact('gallery', 'result');
  //   }else{
  //     // returnjson_encode(array('error'=> "invalid artist_id"));
  //   }
  // }
  //
  // public function by_category(Request $request){
  //   if(!empty($request->input("category_id"))){
  //     $category_id = $request->input("category_id");
  //     $gallery = Gallery::where('category_id', $category_id)->get();
  //     $result = "success";
  //     // returncompact('gallery', 'result');
  //   }else{
  //     // returnjson_encode(array('error'=> "invalid category_id"));
  //   }
  // }
  //
  // public function by_culture(Request $request){
  //   if(!empty($request->input("culture_id"))){
  //     $culture_id = $request->input("culture_id");
  //     $gallery = Gallery::where('culture_id', $culture_id)->get();
  //     $result = "success";
  //     // returncompact('gallery', 'result');
  //   }else{
  //     // returnjson_encode(array('error'=> "invalid culture_id"));
  //   }
  // }


  public function gallery_by_id($id){
     return $post =  PostCategory::find($id);
  }


}
