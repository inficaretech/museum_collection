<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\MainCategory;

use App\Http\Requests;

class MainCategoryapi extends Controller
{
  public function index()
  {
    $main_categories = MainCategory::orderBy('title', 'ASC')->get();
    foreach ($main_categories as $key => $value) {
      $main_categories[$key]->image_url = asset('uploads/main_category').'/'.$value->image_name;
      if(empty($main_categories[$key]->image_type)){
            unset($main_categories[$key]->image_type);
      }
    }
    $result = 'success';
    return compact('main_categories', 'result');
  }

}
