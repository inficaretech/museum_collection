<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;
use App\PostCategory;
use App\MainCategory;
use App\Gallery;
use App\Favorite;
use DB;

use App\Http\Requests;

class SubCategoryapi extends Controller
{
  public function show($id)
  {
    $sub_category = PostCategory::find($id);
    if(!empty($sub_category)){
      $main_categories = MainCategory::find($sub_category->main_categories_id);
      $sub_category->main_categories_name = $main_categories->title;
      $result = 'success';
      return compact('sub_category', 'result');
    }else{
      $error = 'nil';
      return compact('error');
    }
  }



      public function subcategories_gallery_by_id(Request $request){
        $user_id = $request->input('user_id');
        // print_r($user_id);die;
        $posts= array();
        if(!empty($request->input("id"))){
          $id = $request->input("id");
          $sub_category = PostCategory::where('main_categories_id', $id)->get();
          DB::enableQueryLog();

          $galleries = DB::table('galleries')
              ->Join('galleries_post_category', 'galleries_post_category.gallery_id', '=', 'galleries.id')
              ->where( 'galleries_post_category.post_categories_id', '=', $id)
              ->get();
              $queries = DB::getQueryLog();

              $i = 0;
              foreach ($galleries as $key => $value) {
                $post_category[$key]['post_category'] = explode(',', $value->post_category_id);
                $postcat_ids = explode(',', $value->post_category_id);
                foreach ($postcat_ids as $key1 => $value1) {
                  $result= "";
                  $result['cat'] =  $this->gallery_by_id($postcat_ids[$key1]);
                  $result['gallery'] = $value->gallery_id;
                  array_push($posts, $result);
                }
              }
             $post2 = array();
            foreach ($galleries as $key => $value) {
               foreach ($posts as $key2 => $value2) {
                if($value->gallery_id == $value2['gallery'])
                {
                  $post2[$key]['galleries'] = $value;
                  $post2[$key][$key2] = $value2;
                  $ab = $value2['cat'];
                  if($ab->main_categories_id == 1){
                    $post2[$key]['galleries']->artist_id = $ab->name;
                  }
                  elseif ($ab->main_categories_id == 2) {
                    $post2[$key]['galleries']->category_id .= $ab->name.',';
                  }elseif ($ab->main_categories_id == 3) {
                    $post2[$key]['galleries']->culture_id .= $ab->name.',';
                  }
                }
               }
            }

            $result = array();
            $galleries = array();
            foreach ($post2 as $key => $value) {
              $galleries[$key] = $value['galleries'];
              $galleries[$key]->image_url = asset('/uploads/galleries').'/'.$value['galleries']->image_name;
              $galleries[$key]->sub_category_id = $value['galleries']->post_categories_id;
              $galleries[$key]->id = $value['galleries']->gallery_id;
              if(!empty($value['galleries']->artist_id)){
                $galleries[$key]->artist = $value['galleries']->artist_id;
              }else {
                $galleries[$key]->artist = "";
              }
              if(!empty($value['galleries']->category_id)){
                $galleries[$key]->category = $value['galleries']->category_id;
              }else {
                $galleries[$key]->category = "";
              }

              if(!empty($value['galleries']->culture_id)){
                $galleries[$key]->culture = $value['galleries']->culture_id;
              }else{
                $galleries[$key]->culture = "";
              }

              $fav = Favorite::where('user_id', $user_id)->where('gallery_id', $value['galleries']->gallery_id)->get();
              if(count($fav) == 1){
                $galleries[$key]->favorite = '1';
              }else{
                $galleries[$key]->favorite = '0';
              }


              unset($galleries[$key]->main_categories_id);
              unset($galleries[$key]->post_category_id);
              unset($galleries[$key]->created_at);
              unset($galleries[$key]->updated_at);
              unset($galleries[$key]->post_categories_id);
              unset($galleries[$key]->gallery_id);

              unset($galleries[$key]->artist_id);
              unset($galleries[$key]->category_id);
              unset($galleries[$key]->culture_id);
              unset($galleries[$key]->image_type);


              $result['galleries']  = $galleries;
            }
            $success = "true";
            return compact('result', 'success');die;
      }
  }


  public function gallery_by_id($id){
    return $post =  PostCategory::find($id);
  }


  public function all_by_id(Request $request){
     if(!empty($request->input("id"))){
       $id = $request->input("id");
       $sub_category = PostCategory::where('main_categories_id', $id)->get();

       foreach ($sub_category as $key => $value) {
         $main_categories = MainCategory::find($value->main_categories_id);
         $sub_category[$key]->category_name = $main_categories->title;
        //  print_r($sub_category[$key]->feature_image_name);die;
        // return compact('sub_category');die;
        unset($sub_category[$key]->galleries_id);

         if(!empty($sub_category[$key]->feature_image_name)){
           $sub_category[$key]->feature_image = asset('/uploads/sub_category').'/'.$sub_category[$key]->feature_image_name;
        //    $img = Gallery::find($sub_category[$key]->galleries_id);
        //    $sub_category[$key]->feature_image_name = $img->title;
        //    $sub_category[$key]->image_size = $img->image_size;
        //  }else {
        //    $sub_category[$key]->feature_image_name = "";
        //    $sub_category[$key]->feature_image = "";
        //    $sub_category[$key]->image_size = "";
         }
       }

       $result['sub_categories'] = $sub_category;
       $success = "true";
       return compact('result', 'success');
     }else{
       $success = "false";
       return json_encode(array('error'=> "invalid id"));
     }
   }






} // class
