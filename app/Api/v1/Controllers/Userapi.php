<?php

namespace App\Api\v1\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use DB;
use Validator;


class Userapi extends Controller
{
  public function login(Request $request)
  {
    $length = 20;
    $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    $accesstoken = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz"), 0, $length);
    $useremail = $request->input("email");
    $userpassword = md5($request->input("password"));
    $mysql = DB::table('users')->where('email', $useremail)->where('password', $userpassword)->get();
    // return compact('mysql', 'userpassword');die;
    $array = json_decode(json_encode($mysql), true);
    if(count($mysql)==1)
    {
      DB::table('users')->where('id', $array[0]['id'])->update(array('remember_token' => $randomString, 'access_token' => $accesstoken));
      $success = 'true';
      $result['user']['id'] = (int)$array[0]['id'];
      $result['user']['name'] = $array[0]['name'];
      $result['user']['email'] = $array[0]['email'];
      $result['user']['remember_token'] =$randomString;
      $result['user']['access_token'] = $accesstoken;
      $result['message'] = 'Login Successfully';
     }
    //  elseif(count($array)==1 && $array[0]['status']==0)
    // {
    // //  unset($result['user']['access_token']);
    //  $success = 'false';
    //  $error['message'] = 'Please active your account';
    //  return compact('success','error');
    // }
      else
    {
      $success = 'false';
      $result['message'] = 'Invalid email and password';
    }
      return compact('success', 'result');
  }

  public function logout(Request $request)
      {
      $id = $request->input("id");
      $access_token = $request->input("access_token");
      $auth=$this->accesstoken($id,$access_token);
      if($auth==1)
    {
      DB::table('users')->where('id',$id)->update(array('access_token' => ''));
      $result['message'] = 'Logout Successfully';
      $result['status_code'] = '200';
      }
    else
      {
      $result['message'] = 'User Not Access';
      $result['status_code'] = '400';
    }
    return compact( 'result');
  }


  public function storeapi(Request $request)
    {
      $user = new User();

      if(empty($request->input("name"))){
        $success = 'false';
        $result['message'] = 'invalid name';
        return compact('success','result');
      }elseif (empty($request->input("email"))) {
        $success = 'false';
        $result['message'] = 'invalid email';
        return compact('success','result');
      }elseif (empty($request->input("password"))) {
        $success = 'false';
        $result['message'] = 'invalid password';
        return compact('success','result');
      }else{

            $length = 20;
            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
            $accesstoken = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz"), 0, $length);
            $user->name = $request->input("name");
            $user->email = $request->input("email");
            $user->password = md5($request->input("password"));
            $user->remember_token = $randomString;
            $user->access_token = $accesstoken;
            $email_sql=DB::table('users')->where('email',$user->email)->get();
            $email_result=json_decode(json_encode($email_sql),true);
            $result = array();
            $result['user'] = $user;
            if(count($email_sql)==1)
            {
              unset($result['user']['access_token']);
              $success = 'false';
              $result['message'] = 'Email Already Exsists!!';
              return compact('success','result');
            }
            else
            {
                $user->save();
                $success = 'true';
                return compact('success','result');
            }
          }
      }
}
