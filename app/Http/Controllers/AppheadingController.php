<?php namespace App\Http\Controllers;
error_reporting(0);
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Appheading;
use Illuminate\Http\Request;
Use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
class AppheadingController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 public function __construct()
 {
	 if(!Session::has('user'))
	 {
			 Redirect::to('login')->send();
	 }
 }


	public function index()
	{
		$appheadings = Appheading::orderBy('sequence', 'asc')->paginate(30);
		return view('appheadings.index', compact('appheadings'));
	}

	public function appheadings()
	{
		$appheadings = Appheading::orderBy('sequence', 'asc')->paginate(10);
		return compact('appheadings');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('appheadings.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$appheading = new Appheading();

		$appheading->title = $request->input("title");
        $appheading->status = $request->input("status");
        $appheading->font_format = $request->input("font_format");
        $appheading->color = $request->input("color");

		$appheading->save();

		return redirect()->route('appheadings.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$appheading = Appheading::findOrFail($id);

		return view('appheadings.show', compact('appheading'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$appheading = Appheading::findOrFail($id);

		return view('appheadings.edit', compact('appheading'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$appheading = Appheading::findOrFail($id);
		 // DB::enableQueryLog();
		$appheading->title = $request->input("title");
        $appheading->status = $request->input("status-field");
        $appheading->font_format = $request->input("font_format");
        $appheading->color = $request->input("color");
        // print_r($appheading);
		$appheading->save();
		  // echo  dd(DB::getQueryLog());
 // die;
		return redirect()->route('appheadings.index')->with('message', 'Item updated successfully.');
	}

	public function postsequesnce(){
		DB::enableQueryLog();
		$array = explode(",", $_GET['order']);
		// sort($array);
		$ids = $_GET['order'];
		$count = 1;
		foreach ($array as  $idval) {
		$result=DB::table('appheadings')->where('id',$idval)->update(array('sequence'=>$count));
		$count ++;
	}



	return redirect()->route('appheadings.index')->with('message', 'Item updated successfully.');
  }


   public function updatedetails(Request $request){


  	 DB::enableQueryLog();
   		$id = $request->input("id");
   		$app_title = $request->input("app_title");
   		$font_family = $request->input("font_family");
   		$font_size = $request->input("font_size");
   		$title_color = $request->input("title_color");
   		$content_seperator = $request->input("content_seperator");
   		$content_font_family = $request->input("content_font_family");
   		$content_font_size = $request->input("content_font_size");
   		$cont_color = $request->input("cont_color");

   		foreach ($id as $key => $value) {

          if($request->input("status")[$key] == 1){
   	   	    	$status = $request->input("status")[$key];
   	    	}else{
   	   	    	$status =  0;
   	    	}


   	      if($request->input("font_format")[$key] == "Bold"){
   	   	    	$font_format = $request->input("font_format")[$key];
   	    	}elseif($request->input("font_format")[$key] == "Italic"){
   	   	    	$font_format =   $request->input("font_format")[$key];
   	    	}else{
   	    		$font_format = "Normal";
   	    	}

   	     if($request->input("cont_font_format")[$key] == "Bold"){
   	   	    	$cont_font_format = $request->input("cont_font_format")[$key];
   	    	}elseif($request->input("cont_font_format")[$key] == "Italic"){
   	   	    	$cont_font_format =   $request->input("cont_font_format")[$key];
   	    	}else{
   	    		$cont_font_format = "Normal";
   	    	}

   	     if($request->input("all_status")[$key] == 1){
   	   	    	$all_status = $request->input("all_status")[$key];
   	    	}else{
   	   	    	$all_status =  0;
   	    	}

   		$appheading = Appheading::findOrFail($id[$key]);
		$appheading->app_title = $app_title[$key];
		$appheading->status = $status;
		$appheading->font_format = $font_format;
		$appheading->font_family = $font_family[$key];
		$appheading->font_size = $font_size[$key];
		$appheading->color = $title_color[$key];
		$appheading->content_seperator = $content_seperator[$key];
		$appheading->content_font_format = $cont_font_format;
		$appheading->content_font_family = $content_font_family[$key];
		$appheading->content_font_size = $content_font_size[$key];
		$appheading->content_color = $cont_color[$key];
		$appheading->all_status = $all_status;
		$appheading->sequence =
		$appheading->save();

   		}

   		$array = explode(",", $_GET['order']);
		// sort($array);
		$ids = $_GET['order'];
		$count = 1;
	    	foreach ($array as  $idval) {
	     	$result=DB::table('appheadings')->where('id',$idval)->update(array('sequence'=>$count));
	  		$count ++;
			}
		//dd(DB::getQueryLog());
	 return redirect()->route('appheadings.index')->with('message', 'Item updated successfully.');


   }
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$appheading = Appheading::findOrFail($id);
		$appheading->delete();

		return redirect()->route('appheadings.index')->with('message', 'Item deleted successfully.');
	}

}
