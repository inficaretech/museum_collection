<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Artist;
use App\Object;
use App\Medium;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;

class ArtistController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

 //  public function __construct()
 // {
 //  if(!Session::has('user'))
 //  {
 // 		 Redirect::to('login')->send();
 //  }
 // }

	public function index()
	{
		// echo "hello";die;
		$new_artist = array();
		$result = array();
		$artists = DB::connection('mysql_test')->table('Constituents')->get();
		foreach ($artists as $key => $value) {
			 $new_artist[$key] = (array) $value;
			 $img_count = DB::connection('mysql_test')->table('Media')->where('MediaMasterID', $value->m_poster_image)->count();
				if($img_count > 0){
						$image = DB::connection('mysql_test')->table('Media')->where('MediaMasterID', $value->m_poster_image)->get();
						if(!empty($image)){
							$img = $image[0];
							$new_artist[$key]['image'] = $img->FileName;
						}else{
							$new_artist[$key]['image'] = "";
						}
				}
				$result[$key] = (array) $new_artist[$key];

				$art = DB::connection('mysql_test')->table('ObjectConstituentsMaster')->where('ConstituentId', $value->ConstituentID)->get();


				foreach ($art as $k1 => $v1) {

				// return compact('v1');die;
        $objects = DB::connection('mysql_test')->table('Objects')->where('ObjectID', $v1->ObjectId)->get();

				if(count($objects) > 0){
					foreach ($objects as $k1 => $v1) {
						$media = DB::connection('mysql_test')->table('Media')->where('Objectid', $v1->ObjectID)->get();
						// return compact('media');die;
						foreach ($media as $k2 => $v2) {
							# code...
						// print_r($media[0]);die;
            $result[$key]['media'][] = $v2;
					}
					}
				}else{
					$result[$key]['media'][] = "";
				}
			}
		}



		$data['artists'] = $result;
		// return compact('data');die;
		return view('artists.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	// public function create()
	// {
	// 	return view('artists.create');
	// }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	// public function store(Request $request)
	// {
	// 	$artist = new Artist();
	//
	// 	$artist->first_name = $request->input("first_name");
  //       $artist->last_name = $request->input("last_name");
  //       $artist->middle_name = $request->input("middle_name");
  //       $artist->name_title = $request->input("name_title");
  //       $artist->salutation = $request->input("salutation");
  //       $artist->institution = $request->input("institution");
  //       $artist->display_name = $request->input("display_name");
  //       $artist->biography = $request->input("biography");
  //       $artist->artist_type_id = $request->input("artist_type_id");
	//
	// 	$artist->save();
	//
	// 	return redirect()->route('artists.index')->with('message', 'Item created successfully.');
	// }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$new_artist = "";
 		$artist = DB::connection('mysql_test')->table('Constituents')->where('ConstituentID', $id)->get();
		$new_artist = (array) $artist[0];
		$img_count = DB::connection('mysql_test')->table('Media')->where('MediaMasterID', $artist[0]->m_poster_image)->count();
		 if($img_count > 0){
				$image = DB::connection('mysql_test')->table('Media')->where('MediaMasterID', $artist[0]->m_poster_image)->get();
				 if(!empty($image)){
					 $img = $image[0];
					 $new_artist['image'] = $img->FileName;
				 }else{
					 $new_artist['image'] = "";
				 }
		 }
		//  return compact('new_artist');die;
		return view('artists.show', compact('new_artist'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function edit($id)
	// {
	// 	$artist = Artist::findOrFail($id);
	//
	// 	return view('artists.edit', compact('artist'));
	// }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	// public function update(Request $request, $id)
	// {
	// 	$artist = Artist::findOrFail($id);
	//
	// 	$artist->first_name = $request->input("first_name");
  //       $artist->last_name = $request->input("last_name");
  //       $artist->middle_name = $request->input("middle_name");
  //       $artist->name_title = $request->input("name_title");
  //       $artist->salutation = $request->input("salutation");
  //       $artist->institution = $request->input("institution");
  //       $artist->display_name = $request->input("display_name");
  //       $artist->biography = $request->input("biography");
  //       $artist->artist_type_id = $request->input("artist_type_id");
	//
	// 	$artist->save();
	//
	// 	return redirect()->route('artists.index')->with('message', 'Item updated successfully.');
	// }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function destroy($id)
	// {
	// 	$artist = Artist::findOrFail($id);
	// 	$artist->delete();
	//
	// 	return redirect()->route('artists.index')->with('message', 'Item deleted successfully.');
	// }

	public function posterchange(Request $request)
	{
			$id=$request->input("id");
			$insert = DB::connection('mysql_test')->table('Constituents')->where('ConstituentID', $id)->update(['m_poster_image' => $request->input("poster_image")]);
			return redirect()->route('artists.index')->with('Poster', 'Artist Poster updated successfully.');
		}

}
