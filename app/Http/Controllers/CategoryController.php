<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	// public function index()
	// {
	// 	// $categories = DB::connection('mysql_test')->table('Classification')->get();
	// 	$categories = Category::orderBy('id', 'desc')->paginate(10);
	// 	return view('categories.index', compact('categories'));
	// }

	public function __construct()
{
	if(!Session::has('user'))
	{
			Redirect::to('login')->send();
	}
}

	public function index()
	{
		$new_categories = array();
		$result = array();
		$categories = DB::connection('mysql_test')->table('Classification')->get();
		foreach ($categories as $key => $value) {
			 $new_categories[$key] = (array) $value;
			 $img_count = DB::connection('mysql_test')->table('Media')->where('MediaMasterID', $value->m_poster_image)->count();
				if($img_count > 0){
						$image = DB::connection('mysql_test')->table('Media')->where('MediaMasterID', $value->m_poster_image)->get();
						if(!empty($image)){
							$img = $image[0];
							$new_categories[$key]['image'] = $img->FileName;
						}else{
							$new_categories[$key]['image'] = "";
						}
				}
				$result[$key] = (array) $new_categories[$key];
				$objects = DB::connection('mysql_test')->table('Objects')->where('ClassificationID', $value->ClassificationID)->get();
				if(count($objects) > 0){
					foreach ($objects as $k1 => $v1) {
						$media = DB::connection('mysql_test')->table('Media')->where('Objectid', $v1->ObjectID)->get();
						$result[$key]['media'] = $media;
					}
				}else{
					$result[$key]['media'] = "";
				}
		}



		$data['categories'] = $result;
		// return compact('data');die;
		return view('categories.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	// public function create()
	// {
	// 	return view('categories.create');
	// }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	// public function store(Request $request)
	// {
	// 	$category = new Category();
	//
	// 	$category->name = $request->input("name");
  //       $category->sub_category = $request->input("sub_category");
	//
	// 	$category->save();
	//
	// 	return redirect()->route('categories.index')->with('message', 'Item created successfully.');
	// }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function show($id)
	// {
	// 	$category = Category::findOrFail($id);
	// 	return view('categories.show', compact('category'));
	// }


	public function show($id)
	{
		$new_categories = "";
		$categories = DB::connection('mysql_test')->table('Classification')->where('ClassificationID', $id)->get();
		$img_count = DB::connection('mysql_test')->table('Media')->where('MediaMasterID', $categories[0]->m_poster_image)->count();
		 if($img_count > 0){
				$image = DB::connection('mysql_test')->table('Media')->where('MediaMasterID', $categories[0]->m_poster_image)->get();
				 if(!empty($image)){
					 $img = $image[0];
					 $new_categories = (array) $categories[0];
					 $new_categories['image'] = $img->FileName;
				 }else{
					 $new_categories['image'] = "";
				 }
		 }
		//  return compact('new_categories');die;
		return view('categories.show', compact('new_categories'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function edit($id)
	// {
	// 	$category = Category::findOrFail($id);
	//
	// 	return view('categories.edit', compact('category'));
	// }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	// public function update(Request $request, $id)
	// {
	// 	$category = Category::findOrFail($id);
	//
	// 	$category->name = $request->input("name");
  //       $category->sub_category = $request->input("sub_category");
	//
	// 	$category->save();
	//
	// 	return redirect()->route('categories.index')->with('message', 'Item updated successfully.');
	// }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function destroy($id)
	// {
	// 	$category = Category::findOrFail($id);
	// 	$category->delete();
	//
	// 	return redirect()->route('categories.index')->with('message', 'Item deleted successfully.');
	// }

	public function posterchange(Request $request)
	{
			$id=$request->input("id");
			$insert = DB::connection('mysql_test')->table('Classification')->where('ClassificationID', $id)->update(['m_poster_image' => $request->input("poster_image")]);
			return redirect()->route('categories.index')->with('Poster', 'Category Poster updated successfully.');
		}


}
