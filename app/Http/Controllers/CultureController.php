<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Culture;
use Illuminate\Http\Request;

class CultureController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cultures = DB::statement('CALL GetCultures');
		return compact('cultures');die;
		return view('cultures.index', compact('cultures'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	 
	public function create()
	{
		return view('cultures.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$culture = new Culture();

		$culture->name = $request->input("name");

		$culture->save();

		return redirect()->route('cultures.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$culture = Culture::findOrFail($id);

		return view('cultures.show', compact('culture'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$culture = Culture::findOrFail($id);

		return view('cultures.edit', compact('culture'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$culture = Culture::findOrFail($id);

		$culture->name = $request->input("name");

		$culture->save();

		return redirect()->route('cultures.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$culture = Culture::findOrFail($id);
		$culture->delete();

		return redirect()->route('cultures.index')->with('message', 'Item deleted successfully.');
	}

}
