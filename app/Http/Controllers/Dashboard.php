<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
// use Auth;
use App\User;
use App\Map;
use DB;
use App\Category;
use App\Artist;
use App\Culture;
use App\Favorite;
use App\Medium;
use App\Object;
use App\Objectartist;
use App\Objectcategory;
use Session;
use Illuminate\Support\Facades\Redirect;






class Dashboard extends Controller
{

      public function __construct()
    {
      if(!Session::has('user'))
      {
          Redirect::to('login')->send();
      }
    }

    //
    public function index()
    {
      // $data['user'] = Auth::user();

      $data['user'] = "Admin";
      $data['users'] = "1";
      $data['media'] = 1;
      $data['artist'] = 2;
      $data['category'] = 3;

      // $data['users'] = User::where('role', 1)->count();
      // $data['media'] = Medium::all()->count();
      // $data['artist'] = Artist::all()->count();
      // $data['category'] = Category::all()->count();
      return view('dashboard.index', $data);
    }
}
