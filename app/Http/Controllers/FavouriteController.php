<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Favourite;
use Illuminate\Http\Request;

class FavouriteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$favourites = Favourite::orderBy('id', 'desc')->paginate(10);

		return view('favourites.index', compact('favourites'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('favourites.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$favourite = new Favourite();

		$favourite->object_id = $request->input("object_id");
        $favourite->media_master_id = $request->input("media_master_id");
        $favourite->user_id = $request->input("user_id");

		$favourite->save();

		return redirect()->route('favourites.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$favourite = Favourite::findOrFail($id);

		return view('favourites.show', compact('favourite'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$favourite = Favourite::findOrFail($id);

		return view('favourites.edit', compact('favourite'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$favourite = Favourite::findOrFail($id);

		$favourite->object_id = $request->input("object_id");
        $favourite->media_master_id = $request->input("media_master_id");
        $favourite->user_id = $request->input("user_id");

		$favourite->save();

		return redirect()->route('favourites.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$favourite = Favourite::findOrFail($id);
		$favourite->delete();

		return redirect()->route('favourites.index')->with('message', 'Item deleted successfully.');
	}

}
