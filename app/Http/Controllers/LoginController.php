<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Auth;
use App\User;
use Session;
use Illuminate\Support\Facades\Redirect;



class LoginController extends Controller
{

  public function getLogin(){
    return view('login.login');
  }


  public function validateCredentials(Request $request){
    $email = $request->input('email');
    $password = $request->input('password');
    if(!empty($email)){
      if(!empty($password)){
        $auth = $this->authenticate($email, $password);
        if($auth && ($auth != false)){
          $sesson = $this->setsession($auth);
          $user = session()->get('user');
          return Redirect::to('/');
        }else {
          $errors = "Authentication Fails.";
          return redirect()->route('login')->with('message', $errors);
        }
      }else{
        $errors = "Invalid Password";
        return redirect()->route('login')->with('message', $errors);
      }
    }else{
      $errors = "Invalid Email";
      return redirect()->route('login')->with('message', $errors);

    }
  }

  public function authenticate($email, $password) {
    $user = User::where('Email', $email)->where('password', $password)->where('RoleID', 1)->get();
    if (count($user) > 0) {
        return $user;
    } else {
      return false;
    }
  }

  public function setsession($user){
    if(!empty($user)){
      Session::put('user',$user);
      return true;
    }else{
      return false;
    }
  }

  public function logout(Request $request){
    // $request->session()->flash();
    Session::flush();
    return redirect()->route('login');
  }
}
