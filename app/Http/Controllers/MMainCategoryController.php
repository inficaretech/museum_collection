<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MMainCategory;
use Illuminate\Http\Request;
use Image;
use Session;
use Illuminate\Support\Facades\Redirect;


class MMainCategoryController extends Controller {

/**
 * Display a listing of the resource.
 *
 * @return Response
 */

 public function __construct()
{
 if(!Session::has('user'))
 {
		 Redirect::to('login')->send();
 }
}

public function index()
{
	$m_main_categories = MMainCategory::orderBy('id', 'asc')->paginate(10);
  // return compact('m_main_categories');die;
	return view('m_main_categories.index', compact('m_main_categories'));
}

/**
 * Show the form for creating a new resource.
 *
 * @return Response
 */
public function create()
{
	return view('m_main_categories.create');
}

/**
 * Store a newly created resource in storage.
 *
 * @param Request $request
 * @return Response
 */
public function store(Request $request)
{
	$main_category = new MMainCategory();
	$main_category->title = $request->input("title");
	$main_category->text_color = $request->input("text_color");
	$main_category->background_color = $request->input("background_color");
	$file = $request->file('image');
	$imageName = time().'.'.
	$request->file('image')->getClientOriginalExtension();
	$request->file('image')->move(base_path() . '/public/uploads/main_category/', $imageName);
	$image = Image::make(sprintf('uploads/main_category/%s', $imageName))->resize(600, 600)->save();
	$main_category->image_name = $imageName;

	$main_category->save();

	return redirect()->route('m_main_categories.index')->with('message', 'Item created successfully.');
}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return Response
 */
public function show($id)
{
	$main_category = MMainCategory::findOrFail($id);

	return view('m_main_categories.show', compact('main_category'));
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return Response
 */
public function edit($id)
{
	$main_category = MMainCategory::findOrFail($id);

	return view('m_main_categories.edit', compact('main_category'));
}

/**
 * Update the specified resource in storage.
 *
 * @param  int  $id
 * @param Request $request
 * @return Response
 */
public function update(Request $request, $id)
{
	$main_category = MMainCategory::findOrFail($id);

	$main_category->title = $request->input("title");
	$main_category->text_color = $request->input("text_color");
	$main_category->background_color = $request->input("background_color");
	$file = $request->file('image');
	if(!empty($file)){
		$imageName = time().'.'.
		$request->file('image')->getClientOriginalExtension();
		$request->file('image')->move(base_path() . '/public/uploads/main_category/', $imageName);
		$image = Image::make(sprintf('uploads/main_category/%s', $imageName))->resize(600, 600)->save();
		$main_category->image_name = $imageName;
	}

	$main_category->save();

	return redirect()->route('m_main_categories.index')->with('message', 'Item updated successfully.');
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return Response
 */
public function destroy($id)
{
	$main_category = MMainCategory::findOrFail($id);
	$main_category->delete();

	return redirect()->route('m_main_categories.index')->with('message', 'Item deleted successfully.');
}

}
