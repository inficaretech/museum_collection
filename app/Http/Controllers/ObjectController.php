<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// use App\Object;
use Illuminate\Http\Request;
// use App\Objectartist;
// use App\Artist;
// use App\Category;
// use App\Objectcategory;
// use App\Medium;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;

class ObjectController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	 public function __construct()
 {
	 if(!Session::has('user'))
	 {
			 Redirect::to('login')->send();
	 }
 }

	public function index()
	{
		$new_object = array();
		$result = array();
		$objects = DB::table('Objects')->where('PublicAccess', 1)->get();
		// return compact('objects');die;

		foreach ($objects as $key => $value) {
			$category_data = "";
			$artist_data = "";
			$image_url = "";
			 $new_object[$key] = (array) $value;
			 $objectartist = DB::table('ObjectConstituentsMaster')->where('ObjectId', $value->ObjectID)->get();
// return compact('objectartist');die;
			 $objectcategory = DB::table('ObjectClassificationMaster')->where('ObjectId', $value->ObjectID)->get();
			 if(count($objectcategory) > 0){
				 foreach ($objectcategory as $k => $v) {
					$category = DB::table('Classification')->where('ClassificationID', $v->ClassificationId)->get();
					 if(count($category) > 0){
             foreach ($category as $k1 => $v1) {
							$cat = $v1->Classification;
							if($k == 0){
								$category_data .= (string) $cat;
							}else{
								$category_data .= (string) ", ".$cat.",";
							}
             }
					 }else{
						 $category_data .="";
					 }
				 }
			 }
			 else{
				 $category_data .="";
			 }

			//  return compact('category_data');die;

			 if(count($objectartist) > 0){
				 foreach ($objectartist as $k => $v) {
					 $artist = DB::table('Constituents')->where('ConstituentID', $v->ConstituentId)->get();
					 if(count($artist) > 0){
             foreach ($artist as $k1 => $v1) {
							 if(!empty($v1->DisplayName)){
								 if($k == 0){
		 							$artist_data .= $v1->DisplayName;
		 						}else{
		 							$artist_data .= ", ".$v1->DisplayName;
		 						}
							 }else{
								 $artist_data .="";
							 }
             }
					 }else{
						 $artist_data .="";
					 }
				 }
			 }
			 else{
				 $artist_data .="";
			 }

			 if(count($objects) > 0){
				 $media=DB::table('Media')->where('Objectid', $v->ObjectId)->get();
					if(count($media) > 0){
						$med = $media[0];
						//$media
						$medfile = $med->FileName;
						$medfilename = explode("\\", $medfile);
						$filemedia = "";
					    if(count($medfilename) == 1){
					    	$filemedia = $medfilename[0];
					    }elseif(count($medfilename) == 2){
					    	$filemedia = $medfilename[1];
					    }
						$image_url = "http://onlinecollections.anchoragemuseum.org/uploaded_files/".$filemedia;
					}
			 }

			  $new_object[$key]['category'] = $category_data;
				$new_object[$key]['artist'] = $artist_data;
				$new_object[$key]['image_url'] = $image_url;
		}
		// return compact('new_object');die;
		return view('objects.index', compact('new_object'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	// public function create()
	// {
	// 	return view('galleries.create');
	// }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	// public function store(Request $request)
	// {
	// 	$object = new Object();

	// 	$object->artist_id = $request->input("artist_id");
 //        $object->object_id = $request->input("object_id");
 //        $object->object_number = $request->input("object_number");
 //        $object->category_id = $request->input("category_id");
 //        $object->sub_class_id = $request->input("sub_class_id");
 //        $object->date_degin = $request->input("date_degin");
 //        $object->date_end = $request->input("date_end");
 //        $object->object_name = $request->input("object_name");
 //        $object->medium = $request->input("medium");
 //        $object->dimensions = $request->input("dimensions");
 //        $object->signed = $request->input("signed");
 //        $object->inscribed = $request->input("inscribed");
 //        $object->credit_line = $request->input("credit_line");
 //        $object->chat = $request->input("chat");
 //        $object->paper_file_ref = $request->input("paper_file_ref");
 //        $object->exhibitions = $request->input("exhibitions");
 //        $object->provenance = $request->input("provenance");
 //        $object->pub_references = $request->input("pub_references");
 //        $object->curatorial_remarks = $request->input("curatorial_remarks");
 //        $object->related_works = $request->input("related_works");
 //        $object->public_access = $request->input("public_access");
 //        $object->curator_approved = $request->input("curator_approved");
 //        $object->on_view = $request->input("on_view");
 //        $object->entered_date = $request->input("entered_date");
 //        $object->title = $request->input("title");
 //        $object->culture = $request->input("culture");
 //        $object->bibliography = $request->input("bibliography");
 //        $object->dated = $request->input("dated");

	// 	$object->save();

	// 	return redirect()->route('objects.index')->with('message', 'Item created successfully.');
	// }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$new_object = array();
		$category_data = "";
		$artist_data = "";
		$image_url = "";
		$obj = DB::table('Objects')->where('ObjectID', $id)->get();
    $value = (array) $obj;
		$new_object = (array) $value;
		$objectartist = DB::table('ObjectConstituentsMaster')->where('ObjectId', $value[0]->ObjectID)->get();
		$objectcategory = DB::table('ObjectClassificationMaster')->where('ObjectId', $value[0]->ObjectID)->get();
		if(count($objectcategory) > 0){
			foreach ($objectcategory as $k => $v) {
			 $category = DB::table('Classification')->where('ClassificationID', $v->ClassificationId)->get();
				if(count($category) > 0){
					foreach ($category as $k1 => $v1) {
					 $cat = $v1->Classification;
					 if($k == 0){
						 $category_data .= (string) $cat;
					 }else{
						 $category_data .= (string) ", ".$cat;
					 }
					}
				}else{
					$category_data .="";
				}
			}
		}
		else{
			$category_data .="";
		}

		if(count($objectartist) > 0){
			foreach ($objectartist as $k => $v) {
				$artist = DB::table('Constituents')->where('ConstituentID', $v->ConstituentId)->get();
				if(count($artist) > 0){
					foreach ($artist as $k1 => $v1) {
						if(!empty($v1->DisplayName)){
							if($k == 0){
								$artist_data .= $v1->DisplayName;
							}else{
								$artist_data .= ", ".$v1->DisplayName;
							}
						}else {
							$artist_data .="";
						}
					}
				}else{
					$artist_data .="";
				}
			}
		}
		else{
			$artist_data .="";
		}

		if(count($value) > 0){
			$media=DB::table('Media')->where('Objectid', $value[0]->ObjectID)->get();
			 if(count($media) > 0){
				 $med = $media[0];
				 $medfile = $med->FileName;
				 $medfilename = explode("\\", $medfile);
				 $filemedia = "";
					 if(count($medfilename) == 1){
						 $filemedia = $medfilename[0];
					 }elseif(count($medfilename) == 2){
						 $filemedia = $medfilename[1];
					 }
				 $image_url = "http://onlinecollections.anchoragemuseum.org/uploaded_files/".$filemedia;
			 }
		}

		 $new_object['category'] = $category_data;
		 $new_object['artist'] = $artist_data;
		 $new_object['image_url'] = $image_url;

	   $object = (array) $new_object[0];
		 $object['category'] = $category_data;
		 $object['artist'] = $artist_data;
		 $object['image_url'] = $image_url;
	   return view('objects.show', compact('object'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function edit($id)
	// {
	// 	$object = Object::findOrFail($id);
	//
	// 	return view('objects.edit', compact('object'));
	// }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	// public function update(Request $request, $id)
	// {
	// 	$object = Object::findOrFail($id);
	//
	// 	// $object->artist_id = $request->input("artist_id");
  //       // $object->object_id = $request->input("object_id");
  //       $object->object_number = $request->input("object_number");
  //       // $object->category_id = $request->input("category_id");
  //       // $object->sub_class_id = $request->input("sub_class_id");
  //       // $object->date_degin = $request->input("date_degin");
  //       // $object->date_end = $request->input("date_end");
  //       $object->object_name = $request->input("object_name");
  //       $object->medium = $request->input("medium");
  //       $object->dimensions = $request->input("dimensions");
  //       // $object->signed = $request->input("signed");
  //       $object->inscribed = $request->input("inscribed");
  //       $object->credit_line = $request->input("credit_line");
  //       // $object->chat = $request->input("chat");
  //       // $object->paper_file_ref = $request->input("paper_file_ref");
  //       // $object->exhibitions = $request->input("exhibitions");
  //       $object->provenance = $request->input("provenance");
  //       // $object->pub_references = $request->input("pub_references");
  //       // $object->curatorial_remarks = $request->input("curatorial_remarks");
  //       // $object->related_works = $request->input("related_works");
  //       // $object->public_access = $request->input("public_access");
  //       // $object->curator_approved = $request->input("curator_approved");
  //       // $object->on_view = $request->input("on_view");
  //       // $object->entered_date = $request->input("entered_date");
  //       $object->title = $request->input("title");
  //       $object->culture = $request->input("culture");
  //       $object->bibliography = $request->input("bibliography");
  //       $object->dated = $request->input("dated");
	//
	// 	$object->save();
	//
	// 	return redirect()->route('galleries.index')->with('message', 'Item updated successfully.');
	// }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function destroy($id)
	// {
	// 	$object = Object::findOrFail($id);
	// 	$object->delete();
	//
	// 	return redirect()->route('objects.index')->with('message', 'Item deleted successfully.');
	// }

}
