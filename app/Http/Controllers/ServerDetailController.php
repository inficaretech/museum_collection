<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ServerDetail;
use Illuminate\Http\Request;

class ServerDetailController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$server_details = ServerDetail::orderBy('id', 'desc')->paginate(10);

		return view('server_details.index', compact('server_details'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('server_details.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$server_detail = new ServerDetail();

		$server_detail->name = $request->input("name");
        $server_detail->payment_status = $request->input("payment_status");

		$server_detail->save();

		return redirect()->route('server_details.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$server_detail = ServerDetail::findOrFail($id);

		return view('server_details.show', compact('server_detail'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$server_detail = ServerDetail::findOrFail($id);

		return view('server_details.edit', compact('server_detail'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$server_detail = ServerDetail::findOrFail($id);

		$server_detail->name = $request->input("name");
        $server_detail->payment_status = $request->input("payment_status");

		$server_detail->save();

		return redirect()->route('server_details.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$server_detail = ServerDetail::findOrFail($id);
		$server_detail->delete();

		return redirect()->route('server_details.index')->with('message', 'Item deleted successfully.');
	}

}
