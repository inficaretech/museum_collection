<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class VerifyAdmin {

     /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

      public function handle($request, Closure $next)
      {
        if (!empty($request->header('Authorization'))){

          $auth = $request->header('Authorization');
          $new_string = explode('Basic ', $auth);
          $auth_token = $new_string[1];
          $admin = "admin";
          $password = "admin";
          $newauth= $admin.":".$password;
          $auth_s = base64_encode($newauth);
          if ($auth_s != $auth_token)
          {
            return response('Unauthorized.', 401);
          }
        }else{
          return response('Unauthorized.', 401);

        }

          return $next($request);
      }


}
