<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');
//
// Route::get('/', function () {
//     return view('welcome');
// });

// Route::auth();

// Authentication Routes...
// Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'LoginController@logout');
Route::get('login', 'LoginController@getLogin');
Route::post('login', [ 'as' => 'login', 'uses' => 'LoginController@validateCredentials']);

// Route::post('login', 'LoginController@validateCredentials');

// Registration Routes...
// Route::get('register', 'Auth\AuthController@showRegistrationForm');
// Route::post('register', 'Auth\AuthController@register');

// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

// Web Routes //

Route::get('/', 'Dashboard@index');

Route::get('categories/posterchange', 'CategoryController@posterchange');
Route::get('artists/posterchange', "ArtistController@posterchange");
Route::resource("categories","CategoryController",
                ['only' => ['index', 'show']]);
Route::resource("artists","ArtistController",
                ['only' => ['index', 'show']]);
Route::resource("cultures","CultureController",
                ['only' => ['index', 'show']]);
Route::resource("favourites","FavouriteController");
Route::resource("media","MediumController",
                ['only' => ['index', 'show']]);
// Route::resource("objectcategories","ObjectcategoryController");
// Route::resource("objectartists","ObjectartistController");
Route::resource("galleries","ObjectController",
                ['only' => ['index', 'show']]);


// Route::get('map', 'MappingController@map');
// Route::resource("main_categories","MainCategoryController");
Route::resource("m_main_categories","MMainCategoryController"); // Add this line in routes.php
Route::get('appheadings/postsequesnce', 'AppheadingController@postsequesnce');
Route::get('appheadings/appheadings', 'AppheadingController@appheadings');
Route::post('appheadings/updatedetails', 'AppheadingController@updatedetails');
Route::resource("appheadings","AppheadingController");

// Route::get('artist/posterchange', 'ArtistController@posterchange');
// Route::get('artist/posterchange', 'ArtistController@posterchange');
// Route::resource("server_details","ServerDetailController");
// Route::resource("galleries","GalleryController");
// Route::resource("main_categories","MainCategoryController");
// Route::resource("cultures","CultureController");
// Route::resource("categories","CategoryController");
// Route::resource("app_details","AppDetailController");
// Route::resource("artists","ArtistController");
// Route::resource("favorites","FavoriteController");
// Route::resource("basic_auths","BasicAuthController");
// Route::resource("post_categories","PostCategoryController");
//
// Route::get("cultures","PostCategoryController@cultures_index");
// Route::get("artists","PostCategoryController@artists_index");
// Route::get("categories","PostCategoryController@categories_index");



// API Routes //

// $api->version('v1', function ($api) {
//
//   $api->get('maincategory/','App\Api\v1\Controllers\MainCategoryapi@index')->middleware('auth.admin');
//   $api->get('subcategory/data_by_id','App\Api\v1\Controllers\SubCategoryapi@all_by_id')->middleware('auth.admin');
//   $api->get('galleries','App\Api\v1\Controllers\Galleryapi@index')->middleware('auth.admin');
//   $api->get('galleries/by_subcategory','App\Api\v1\Controllers\SubCategoryapi@subcategories_gallery_by_id')->middleware('auth.admin');
//   $api->get('galleries/gallery_search','App\Api\v1\Controllers\Galleryapi@gallery_search')->middleware('auth.admin');
//   $api->get('galleries/all_subcategory','App\Api\v1\Controllers\Galleryapi@all_subcategory')->middleware('auth.admin');
//   $api->post('favorites','App\Api\v1\Controllers\Favoriteapi@store')->middleware('auth.admin');
//   $api->get('favorites/userfavorite','App\Api\v1\Controllers\Favoriteapi@user_favorite')->middleware('auth.admin');
//   $api->post('users/login','App\Api\v1\Controllers\Userapi@login')->middleware('auth.admin');
//   $api->get('users/logout','App\Api\v1\Controllers\Userapi@logout')->middleware('auth.admin');
//   $api->post('users/','App\Api\v1\Controllers\Userapi@storeapi')->middleware('auth.admin');
//
//
//
//
//   // $api->post('galleries','App\Api\v1\Controllers\Galleryapi@store')->middleware('auth.admin');
//   // $api->get('galleries/by_artist','App\Api\v1\Controllers\Galleryapi@by_artist')->middleware('auth.admin');
//   // $api->get('galleries/by_category','App\Api\v1\Controllers\Galleryapi@by_category')->middleware('auth.admin');
//   // $api->get('galleries/by_culture','App\Api\v1\Controllers\Galleryapi@by_culture')->middleware('auth.admin');
//
//
//
//
//   $api->get('galleries/{gallery_id}','App\Api\v1\Controllers\Galleryapi@show')->middleware('auth.admin');
//   $api->get('subcategory/{subcategory_id}','App\Api\v1\Controllers\SubCategoryapi@show')->middleware('auth.admin');
//   // $api->get('artists/','App\Api\v1\Controllers\Artistapi@index')->middleware('auth.admin');
//   // $api->get('cultures/','App\Api\v1\Controllers\Cultureapi@index')->middleware('auth.admin');
//   // $api->get('categories/','App\Api\v1\Controllers\Categoryapi@index')->middleware('auth.admin');
//   // $api->get('artists/{artist_id}','App\Api\v1\Controllers\Artistapi@show')->middleware('auth.admin');
//   // $api->get('cultures/{culture_id}','App\Api\v1\Controllers\Cultureapi@show')->middleware('auth.admin');
//   // $api->get('categories/{category_id}','App\Api\v1\Controllers\Categoryapi@show')->middleware('auth.admin');
//
//
// });
