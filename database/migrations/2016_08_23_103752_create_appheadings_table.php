<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppheadingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('appheadings', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->boolean('status')->default(1);
            $table->string('font_format');
            $table->string('color');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('appheadings');
	}

}
