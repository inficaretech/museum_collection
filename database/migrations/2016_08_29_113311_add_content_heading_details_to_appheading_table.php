<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContentHeadingDetailsToAppheadingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appheadings', function (Blueprint $table) {
            $table->string('font_family')->after('color')->nullable();
            $table->string('font_size')->after('font_family')->nullable();
            $table->string('content_font_format')->after('font_size')->nullable();
            $table->string('content_font_family')->after('content_font_format')->nullable();
            $table->string('content_font_size')->after('content_font_family')->nullable();
            $table->string('content_color')->after('content_font_size')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appheadings', function (Blueprint $table) {
            //
        });
    }
}
