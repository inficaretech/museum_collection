<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSequenceTypeToAppheadingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up($autoIncrement = false)
    {
        Schema::table('appheadings', function (Blueprint $table) {
             // $table->integer('sequence', 22)->nullable()->change();
            $table->integer('sequence', false, true)->length(10)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appheadings', function (Blueprint $table) {
            //
        });
    }
}
