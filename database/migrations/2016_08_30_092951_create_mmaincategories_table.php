<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMMainCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_main_categories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('text_color');
            $table->string('background_color');
            $table->string('image_name');
            $table->string('image_type');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_main_categories');
	}

}
