<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContentSeperatorToAppheadingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appheadings', function (Blueprint $table) {
           $table->string('content_seperator')->after('font_size')->default(':');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appheadings', function (Blueprint $table) {
            //
        });
    }
}
