<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class MMainCategoryTableSeeder extends Seeder {

    public function run()
    {
      DB::table('m_main_categories')->insert([
        [
        'title' => 'Artist',
        'background_color' => '#fec107',
        'text_color' => '#ffffff',
        'created_at' =>  date("Y-m-d H:i:s"),
        'updated_at' =>  date("Y-m-d H:i:s")
      ],

      [
        'title' => 'Categories',
        'background_color' => '#ff1f1f',
        'text_color' => '#ffffff',
        'created_at' =>  date("Y-m-d H:i:s"),
        'updated_at' =>  date("Y-m-d H:i:s")
      ],

      [
        'title' => 'Culture',
        'background_color' => '#02b5cc',
        'text_color' => '#ffffff',
        'created_at' =>  date("Y-m-d H:i:s"),
        'updated_at' =>  date("Y-m-d H:i:s")
      ],

      [
        'title' => 'Favorites',
        'background_color' => '0eae33',
        'text_color' => '#ffffff',
        'created_at' =>  date("Y-m-d H:i:s"),
        'updated_at' =>  date("Y-m-d H:i:s")
      ]
      ]);    }

}
