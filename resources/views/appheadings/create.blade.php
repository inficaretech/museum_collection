@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Appheadings / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('appheadings.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label for="title-field">Title</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ old("title") }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('status')) has-error @endif">
                       <label for="status-field">Status</label>
                    <div class="btn-group" data-toggle="buttons"><label class="btn btn-primary"><input type="radio" value="true" name="status-field" id="status-field" autocomplete="off"> True</label><label class="btn btn-primary active"><input type="radio" name="status-field" value="false" id="status-field" autocomplete="off"> False</label></div>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('font_format')) has-error @endif">
                       <label for="font_format-field">Font_format</label>
                    <input type="text" id="font_format-field" name="font_format" class="form-control" value="{{ old("font_format") }}"/>
                       @if($errors->has("font_format"))
                        <span class="help-block">{{ $errors->first("font_format") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('color')) has-error @endif">
                       <label for="color-field">Color</label>
                    <input type="text" id="color-field" name="color" class="form-control" value="{{ old("color") }}"/>
                       @if($errors->has("color"))
                        <span class="help-block">{{ $errors->first("color") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('appheadings.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
