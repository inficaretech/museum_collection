@extends('layouts.app')
@section('content')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


  <div class="right_col" role="main">
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Edit Appheadings</h1>
    </div>


    @include('error')
    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('appheadings.update', $appheading->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label for="title-field">Title</label>

                        <!-- <a class="pUpdate" data-pk="{{$appheading->id}}">{{ is_null(old("title")) ? $appheading->title : old("title") }}</a> -->
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ is_null(old("title")) ? $appheading->title : old("title") }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>


                    <div class="form-group @if($errors->has('status')) has-error @endif">
                       <label for="status-field">Status</label>
                    <div class="btn-group" data-toggle="buttons">
                     @if($appheading->status == 1) 
                       <label class="btn btn-primary active"><input type="radio" value="1" name="status-field" id="status-field" autocomplete="off"> Enable</label>
                      @else
                      <label class="btn btn-primary"><input type="radio" value="1" name="status-field" id="status-field" autocomplete="off"> Enable</label>
                      @endif

                        @if($appheading->status == 0) 
                        <label class="btn btn-primary active "><input type="radio" name="status-field" value="0" id="status-field" autocomplete="off"> Disable</label>
                         @else
                         <label class="btn btn-primary  "><input type="radio" name="status-field" value="0" id="status-field" autocomplete="off"> Disable</label>
                        @endif
                     </div>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>



                    <div class="form-group @if($errors->has('font_format')) has-error @endif">
                       <label for="font_format-field">Font_format</label>
                      <select id="font_format-field" name="font_format"  class="form-control" >
                      <option value="Normal"   @if($appheading->font_format == "Normal") {{"selected"}} @endif >Normal</option>
                      <option value="Bold"  @if($appheading->font_format == "Bold") {{"selected"}} @endif>Bold</option>
                      <option value="Italic"  @if($appheading->font_format == "Italic") {{"selected"}} @endif>Italic</option>
                      </select>

                    <!-- <input type="text" id="font_format-field" name="font_format" class="form-control" value="{{ is_null(old("font_format")) ? $appheading->font_format : old("font_format") }}"/> -->
                       @if($errors->has("font_format"))
                        <span class="help-block">{{ $errors->first("font_format") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('color')) has-error @endif">
                       <label for="color-field">Color</label>
                    <input type="color" id="color-field" name="color" class="form-control" value="{{ is_null(old("color")) ? $appheading->color : old("color") }}"/>
                       @if($errors->has("color"))
                        <span class="help-block">{{ $errors->first("color") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('appheadings.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>

    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
