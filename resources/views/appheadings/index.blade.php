@extends('layouts.app')
@section('content')
            <form action="appheadings/updatedetails" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

  <div class="right_col" role="main">

    <div class="page-header clearfix">
        <h5>
Use this screen designer to show/hide fields and display characteristics of titles and content.

To change the order in which information appears, drag and drop rows. 
        </h5>
 <div><input type="submit" name="submit" value="Update Details" class="send-btn" />
      <!-- <a href="appheadings" > <input type="button" value="Update Sequence"> </a> -->
      </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if($appheadings->count())
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="heading_table">
<thead>
<tr>
<td align="center"><strong>S. NO.</strong></td>
<td colspan="7" align="center"><strong>Title</strong></td>
<td colspan="5" align="center"><strong>Content</strong></td>
<td  align="center"><strong>Show/Hide</strong></td>
</tr>
<tr>
<td align="center">&nbsp;</td>
<td align="center">DB Field</td>
<td align="center">Title</td>
<td align="center">Show/Hide</td>
<td align="center">Font Format</td>
<td align="center">Font Family</td>
<td align="center">Size</td>
<td align="center">Color</td>
<td align="center">Seperator</td>
<td align="center">Font Format</td>
<td align="center">Font Family</td>
<td align="center">Size</td>
<td align="center">Color</td>
<!-- <td align="center">&nbsp;</td> -->
<td align="center"><!-- <input type="checkbox" id="checkedAll" > --></td>

    </tr>
    </thead>
    <tbody id = "order">
     @foreach($appheadings  as $key => $appheading)
      <tr id="{{$appheading->id}}">
      <input type="hidden" class="id" name="id[]" value="{{$appheading->id}}">
      <td align="center" class="id">{{$appheading->sequence}}</td>
      <td align="center">{{$appheading->title}}</td>
      <td align="center"><input type="text" class="title" value="{{$appheading->app_title}}" name="app_title[{{$key}}]"/></td>
      <td align="center"><input type="checkbox" id="status{{$appheading->id}}" name="status[{{$key}}]" value = "1" <?php if($appheading->status ==1){ echo "checked"; }?> ></td>
      <td align="center">
        <label><input type="checkbox" class='unique check{{$appheading->id}}' name="font_format[{{$key}}]" value="Bold" <?php if($appheading->font_format == "Bold"){ echo "checked";}?> > Bold</label>
        <label><input type="checkbox" class='unique check{{$appheading->id}}' name="font_format[{{$key}}]" value="Italic" <?php if($appheading->font_format == "Italic"){ echo "checked";}?> /> Italic</label>
      </td>
     
      <td align="center">
        <select name="font_family[{{$key}}]">
        <option value="Arial" <?php if($appheading->font_family == "Arial"){ echo "selected";}?> >Arial</option>
        <option value="Times new roman"  <?php if($appheading->font_family == "Times new roman"){ echo "selected";}?>>Times new roman</option>
        </select>
      </td>
      <td align="center">
      <select name="font_size[{{$key}}]">
        <option value="8px" <?php if($appheading->font_size == "8px"){ echo "selected";}?> >8px</option>
        <option value="10px" <?php if($appheading->font_size == "10px"){ echo "selected";}?> >10px</option>
      </select></td>
      <td align="center"><input type="color" name="title_color[{{$key}}]" value="{{$appheading->color}}"></td>
      <td align="center">
      <select name="content_seperator[{{$key}}]">
         <option value=":" <?php if($appheading->content_seperator == ":"){ echo "selected";}?>>:</option>
         <option value=";" <?php if($appheading->content_seperator == ";"){ echo "selected";}?>>;</option>
         <option value="-" <?php if($appheading->content_seperator == "-"){ echo "selected";}?>>-</option>
         <option value="space" <?php if($appheading->content_seperator == "space"){ echo "selected";}?>>space</option>
      </select>
      </td>
      <td align="center">
        <label><input type="checkbox" class='uniquecont checkcont{{$appheading->id}}' name="cont_font_format[{{$key}}]" value="Bold" <?php if($appheading->content_font_format == "Bold"){ echo "checked";}?> > Bold</label>
        <label><input type="checkbox" class='uniquecont checkcont{{$appheading->id}}' name="cont_font_format[{{$key}}]" value="Italic" <?php if($appheading->content_font_format == "Italic"){ echo "checked";}?> /> Italic</label>
      </td>
      <td align="center">
      <select name="content_font_family[{{$key}}]">
         <option value="Arial" <?php if($appheading->content_font_family == "Arial"){ echo "selected";}?>>Arial</option>
         <option value="Times new roman" <?php if($appheading->content_font_family == "Times new roman"){ echo "selected";}?>>Times new roman</option>
      </select>
      </td>
      <td align="center">
       <select name="content_font_size[{{$key}}]">
         <option value="8px" <?php if($appheading->content_font_size == "8px"){ echo "selected";}?> >8px</option>
         <option value="10px" <?php if($appheading->content_font_size == "10px"){ echo "selected";}?> >10px</option>
       </select>
       </td>
      <td align="center"><input type="color" name="cont_color[{{$key}}]" value="{{$appheading->content_color}}"></td>
      
     <!--  <td align="center">Update details</td> -->
      <td align="center"><input type="checkbox" id="{{$appheading->id}}"  name="all_status[{{$key}}]"  value = "1" class="checkSingle" <?php if($appheading->all_status == 1){ echo "checked";}?>  /></td>
    </tr>
     @endforeach
  </tbody>
</table>
   @else
      <h3 class="text-center alert alert-info">Empty!</h3>
  @endif
   </div>
    </div>

    </div>
 </form>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script type="text/javascript">


// update sequence ajax
$(document).ready(function() 
{
  $("tbody#order").sortable({   
  update : function () 
    { 
       var order = $(this).sortable('toArray');
       var dataString = 'order='+ order
        $.ajax({
            type: 'GET',
            url: 'appheadings/postsequesnce',
            data: dataString,
            dataType: 'html',
            success: function (html) {
            setTimeout(function() 
            {
            location.reload();  //Refresh page
            }, 10);
            return true;
      
            },
            // error: alert('fail')
        });
    }
    });  
});

// font format checkbox for title
$('input.unique').each(function() {
    $(this).on('touchstart click', function() {
        var secondClass = $(event.target).attr('class').split(' ')[1];
        $('input.'+secondClass).not(this).removeAttr('checked');
    });
});


// font format checkbox for content
$('input.uniquecont').each(function() {
    $(this).on('touchstart click', function() {
        var secondClass = $(event.target).attr('class').split(' ')[1];
        $('input.'+secondClass).not(this).removeAttr('checked');
    });
});


// checkall script
$(document).ready(function() {
  $("#checkedAll").change(function(){
    if(this.checked){
      $(".checkSingle").each(function(){
        this.checked=true;
      })              
    }else{
      $(".checkSingle").each(function(){
        this.checked=false;
      })              
    }
  });

  $(".checkSingle").click(function () {
    if ($(this).is(":checked")){
      var isAllChecked = 0;
      var thisid = $(this).attr('id');
      var statusid = '#status'+thisid;

      $(statusid).prop("checked", true);
      $(".checkSingle").each(function(){
        if(!this.checked)
           isAllChecked = 1;
      })              
      if(isAllChecked == 0){ $("#checkedAll").prop("checked", true); }     
    }else {
     var thisid = $(this).attr('id');
     var statusid = '#status'+thisid;
      $(statusid).prop("checked", false);
      $("#checkedAll").prop("checked", false);
    }
  });
});

</script>
@endsection
