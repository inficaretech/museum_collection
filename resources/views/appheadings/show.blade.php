@extends('layout')
@section('header')
<div class="page-header">
        <h1>Appheadings / Show #{{$appheading->id}}</h1>
        <form action="{{ route('appheadings.destroy', $appheading->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('appheadings.edit', $appheading->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="title">TITLE</label>
                     <p class="form-control-static">{{$appheading->title}}</p>
                </div>
                    <div class="form-group">
                     <label for="status">STATUS</label>
                     <p class="form-control-static">{{$appheading->status}}</p>
                </div>
                    <div class="form-group">
                     <label for="font_format">FONT_FORMAT</label>
                     <p class="form-control-static">{{$appheading->font_format}}</p>
                </div>
                    <div class="form-group">
                     <label for="color">COLOR</label>
                     <p class="form-control-static">{{$appheading->color}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('appheadings.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection