@extends('layouts.app')
@section('content')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection

<div class="right_col" role="main">

    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Artists / Create </h1>
    </div>

    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('artists.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('first_name')) has-error @endif">
                       <label for="first_name-field">First_name</label>
                    <input type="text" id="first_name-field" name="first_name" class="form-control" value="{{ old("first_name") }}"/>
                       @if($errors->has("first_name"))
                        <span class="help-block">{{ $errors->first("first_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('last_name')) has-error @endif">
                       <label for="last_name-field">Last_name</label>
                    <input type="text" id="last_name-field" name="last_name" class="form-control" value="{{ old("last_name") }}"/>
                       @if($errors->has("last_name"))
                        <span class="help-block">{{ $errors->first("last_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('middle_name')) has-error @endif">
                       <label for="middle_name-field">Middle_name</label>
                    <input type="text" id="middle_name-field" name="middle_name" class="form-control" value="{{ old("middle_name") }}"/>
                       @if($errors->has("middle_name"))
                        <span class="help-block">{{ $errors->first("middle_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('name_title')) has-error @endif">
                       <label for="name_title-field">Name_title</label>
                    <input type="text" id="name_title-field" name="name_title" class="form-control" value="{{ old("name_title") }}"/>
                       @if($errors->has("name_title"))
                        <span class="help-block">{{ $errors->first("name_title") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('salutation')) has-error @endif">
                       <label for="salutation-field">Salutation</label>
                    <input type="text" id="salutation-field" name="salutation" class="form-control" value="{{ old("salutation") }}"/>
                       @if($errors->has("salutation"))
                        <span class="help-block">{{ $errors->first("salutation") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('institution')) has-error @endif">
                       <label for="institution-field">Institution</label>
                    <input type="text" id="institution-field" name="institution" class="form-control" value="{{ old("institution") }}"/>
                       @if($errors->has("institution"))
                        <span class="help-block">{{ $errors->first("institution") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('display_name')) has-error @endif">
                       <label for="display_name-field">Display_name</label>
                    <input type="text" id="display_name-field" name="display_name" class="form-control" value="{{ old("display_name") }}"/>
                       @if($errors->has("display_name"))
                        <span class="help-block">{{ $errors->first("display_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('biography')) has-error @endif">
                       <label for="biography-field">Biography</label>
                    <input type="text" id="biography-field" name="biography" class="form-control" value="{{ old("biography") }}"/>
                       @if($errors->has("biography"))
                        <span class="help-block">{{ $errors->first("biography") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('artist_type_id')) has-error @endif">
                       <label for="artist_type_id-field">Artist_type_id</label>
                    <input type="text" id="artist_type_id-field" name="artist_type_id" class="form-control" value="{{ old("artist_type_id") }}"/>
                       @if($errors->has("artist_type_id"))
                        <span class="help-block">{{ $errors->first("artist_type_id") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('artists.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
  </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
