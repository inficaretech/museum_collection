@extends('layouts.app')
@section('content')

<div class="right_col" role="main">

    <div class="page-header clearfix">
        <h1>
            <i class="fa fa-user"></i> Artists

            <div id="myModal" class="modal">

              <!-- Modal content -->
              <div class="modal-content">
                <span class="close">x</span>
                <div style="width:317px; margin:0 auto; background:#eee; padding:10px;">
                  <table width="100%" border="0" cellspacing="5" cellpadding="0" style="background:#fff;">
                    <tbody>
                      <tr>


                        <td valign="top" bgcolor="#666666" style="border:#000 solid 1px;">
                          <?php if(!empty($artists[0]['m_poster_image'])) { ?>
                        	<div><img src="http://onlinecollections.anchoragemuseum.org/uploaded_files/<?php echo $artists[0]['image']; ?>" width="149" height="110" alt=""/></div>
                          <?php } else { ?>
                            <div><img src="{{ asset( './uploads/default_artist/1.jpg' ) }}" width="149" height="110" alt=""/>

                            </div>
                            <?php } ?>
                          <div style="font-family:arial; padding:3px; font-size:12px; color:#fff; text-align:center;"><?php echo $artists[0]['DisplayName'];?><span style="font-family:arial; display:block; font-size:10px; color:#fff; text-align:center;">Maddona of Jerome</span></div>
                        </td>

                        <td rowspan="2" valign="top" bgcolor="#666666" style="border:#000 solid 1px;">
                          <?php if(!empty($artists[1]['m_poster_image'])) { ?>
                        <div><img src="http://onlinecollections.anchoragemuseum.org/uploaded_files/<?php echo $artists[1]['image']; ?>" width="149" height="266" alt=""/></div>
                        <?php } else { ?>
                          <div><img src="{{ asset( './uploads/default_artist/2.jpg' ) }}" width="149" height="110" alt=""/></div>
                          <?php } ?>
                          <div style="font-family:arial; padding:3px; font-size:12px; color:#fff; text-align:center;"><?php echo $artists[1]['DisplayName'];?><span style="font-family:arial; display:block; font-size:10px; color:#fff; text-align:center;">Maddona of Jerome</span></div>
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" bgcolor="#666666" style="border:#000 solid 1px;">
                          <?php if(!empty($artists[2]['m_poster_image'])) { ?>
                          <div><img src="http://onlinecollections.anchoragemuseum.org/uploaded_files/<?php echo $artists[2]['image']; ?>" width="149" height="110" alt=""/></div>
                          <?php } else { ?>
                            <div><img src="{{ asset( './uploads/default_artist/3.jpg' ) }}" width="149" height="110" alt=""/></div>
                            <?php } ?>
                         <div style="font-family:arial; padding:3px; font-size:12px; color:#fff; text-align:center;"><?php echo $artists[2]['DisplayName'];?><span style="font-family:arial; display:block; font-size:10px; color:#fff; text-align:center;">Maddona of Jerome</span></div>
                        </td>
                        </tr>
                      <tr>
                        <td valign="top" bgcolor="#666666" style="border:#000 solid 1px;">
                          <?php if(!empty($artists[3]['m_poster_image'])) { ?>
                        <div><img src="http://onlinecollections.anchoragemuseum.org/uploaded_files/<?php echo $artists[3]['image']; ?>" width="149" height="110" alt=""/></div>
                        <?php } else { ?>
                          <div><img src="{{ asset( './uploads/default_artist/4.jpg' ) }}" width="149" height="110" alt=""/></div>
                          <?php } ?>
                          <div style="font-family:arial; padding:3px; font-size:12px; color:#fff; text-align:center;"><?php echo $artists[3]['DisplayName'];?><span style="font-family:arial; display:block; font-size:10px; color:#fff; text-align:center;">Maddona of Jerome</span></div>
                        </td>
                        <td valign="top" bgcolor="#666666" style="border:#000 solid 1px;">
                          <?php if(!empty($artists[4]['m_poster_image'])) { ?>
                        <div><img src="http://onlinecollections.anchoragemuseum.org/uploaded_files/<?php echo $artists[4]['image']; ?>" width="149" height="110" alt=""/></div>
                        <?php } else { ?>
                          <div><img src="{{ asset( './uploads/default_artist/5.jpg' ) }}" width="149" height="110" alt=""/></div>
                          <?php } ?>
                          <div style="font-family:arial; padding:3px; font-size:12px; color:#fff; text-align:center;"><?php echo $artists[4]['DisplayName'];?><span style="font-family:arial; display:block; font-size:10px; color:#fff; text-align:center;">Maddona of Jerome</span></div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </h1>

    </div>

    <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                    <button id="myBtn" class="btn btn-small">Preview</button>

                    <div class="clearfix"></div>
                  </div>
                <table id="datatable" class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <td>Image</td>
                            <th>Feature Image</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>
                    <tbody>

                  <?php foreach ($artists as $key => $artist) {

                    // print_r($artist);
                  ?>

                  <tr>
                      <td><?php echo $key+1; ?></td>

                      <?php if($artist['DisplayName']) { ?>
                        <td><?php echo $artist['DisplayName'] ?></td>
                      <?php }elseif($artist['FirstName'] || $artist['MiddleName'] || $artist['LastName']) { ?>
                        <td><?php echo $artist['FirstName']." ".$artist['MiddleName']." ".$artist['LastName']; ?></td>
                      <?php } else { ?>
                        <td>N/A</td>
                        <?php } ?>

                          <td>
                            <form id="myForm" method="GET" action="artist/posterchange">
                              <input type="hidden" name="id" id="mySelect" value="<?php echo $artist['ConstituentID'] ?>"/>
                              <select class="my-select" name="poster_image" id="my-select1" onchange="this.form.submit()">
                                <option value="">Select Image</option>
                               <?php

                                if(!empty($artist['media'])) {


                                  foreach ($artist['media'] as $k1 => $v1) {
                                    # code...
                                  // for ($i=0; $i < count($artist['media']); $i++) {
                                    // print_r($artist['media']);die;
                                    if(!empty($v1->RenditionNumber)) {

                                ?>
                                      <option data-img-src="http://onlinecollections.anchoragemuseum.org/uploaded_files/<?php echo $v1->FileName; ?>" value="<?php echo $v1->MediaMasterID; ?>">&nbsp;</option>
                                    <?php  } else { ?>
                                      <option value="">N/A</option>
                                    <?php }
                                  // }
                                }
                              }
                                    ?>

                              </select>
                    </form>
                  </td>


                          <?php if(!empty($artist['m_poster_image'])) { ?>
                              <td class="profile_pic"><img src="http://onlinecollections.anchoragemuseum.org/uploaded_files/<?php echo $artist['image'] ?>" class="image-class" id= "img-class" style="width:60px;"/></td>
                            <?php } else { ?>
                            <td class="profile_pic"><img src="{{ asset( './uploads/missing_image.png' ) }}" class="image-class" id= "img-class" style="width:60px;"/></td>
                            <?php } ?>


                        <?php $id =  $artist['ConstituentID']?>
                    <td class="text-right">
                        <a class="btn btn-xs btn-primary" href="{{ route('artists.show', $id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                    </td>

                  </tr>
                  <?php

                   } ?>
                 </tbody>
              </table>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .big_table{ overflow: auto;}
</style>

</script>
<script type="text/javascript">
$(".my-select").chosen({width:"100%"});
</script>

<script>
$(document).ready(function() {
$('#datatable').DataTable({
        stateSave: true
});
});
</script>


<script>
    // Get the modal
  var modal = document.getElementById('myModal');

  // Get the button that opens the modal
  var btn = document.getElementById("myBtn");

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks on the button, open the modal
  btn.onclick = function() {
      modal.style.display = "block";
  }

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
      modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal) {
          modal.style.display = "none";
      }
  }
</script>

@endsection
