@extends('layouts.app')
@section('content')

<div class="right_col" role="main">

<div class="page-header">
        <h1>Artists / Show </h1>

    </div>
    <div class="x_panel">
      <div class="x_title">
              <ul class="nav navbar-right panel_toolbox">
              </ul>
              <div class="clearfix"></div>
            </div>
    <div class="row">
        <div class="col-md-6 view_page">
            <form action="#">
              <table class="table table-condensed table-striped">
                      <tr>
                        <td><b>Name: </b></td>
                        <?php if(!empty($new_artist['DisplayName'])) { ?>
                          <td><?php echo $new_artist['DisplayName'];?></td>
                        <?php }else { ?>
                          <td>N/A</td>
                        <?php } ?>
                      </tr>
                      <tr>
                        <td><b>Name Title: </b></td>
                        <?php if(!empty($new_artist['NameTitle'])) { ?>
                        <td><?php echo $new_artist['NameTitle']; ?></td>
                        <?php }else { ?>
                        <td>N/A</td>
                        <?php } ?>
                      </tr>
                      <tr>
                        <td><b>Salutation: </b></td>
                        <?php if(!empty($new_artist['Salutation'])) { ?>
                        <td><?php echo $new_artist['Salutation']; ?></td>
                        <?php }else { ?>
                        <td>N/A</td>
                        <?php } ?>
                      </tr>
                      <tr>
                        <td><b>Institution: </b></td>
                        <?php if(!empty($new_artist['Institution'])) { ?>
                        <td> <?php echo $new_artist['Institution']; ?></td>
                        <?php }else { ?>
                        <td>N/A</td>
                        <?php } ?>
                      </tr>
                      <tr>
                        <td><b>Biography: </b></td>
                        <?php if(!empty($new_artist['Biography'])) { ?>
                        <td><?php echo  $new_artist['Biography']; ?></td>
                        <?php }else { ?>
                        <td>N/A</td>
                        <?php } ?>
                      </tr>
                  </table>
              </form>
        </div>
        <div class="col-md-6 view_page">
          <table class="table table-condensed table-striped">
              <tr>
                 <?php if(!empty($new_artist['image'])) {?>

                <td style="width:100%;" class="profile_pic"><img src="http://onlinecollections.anchoragemuseum.org/uploaded_files/<?php echo $new_artist['image']; ?>" class="image-class" style="width:50%; height:auto; border:#ccc solid 1px; padding:1px;" /></td>
                <?php }else { ?>
                <td class="profile_pic"><img src="{{ asset( './uploads/missing_image.png' ) }}" class="image-class" id= "img-class" style="width:100%;"/></td>
               <?php } ?>
            </tr>
          </table>
      </div>
    </div>
    <a class="btn btn-link" href="{{ route('artists.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
</div>
</div>
@endsection
