@extends('layouts.blank')

@section('content')

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
              {{ csrf_field() }}
              <h1>Login </h1>

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <div>
                <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required="">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div>
                  <input id="password" type="password" placeholder="Password" class="form-control" name="password" required="">

                <!-- <input class="form-control" placeholder="Password" required="" type="password"> -->
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div>
              <div>
                <!-- <span> -->
                <!-- <input type="checkbox" name="remember"> Remember Me -->
              <!-- </span class = "help-block"> -->
              <button type="submit" class="btn btn-default submit">
                  <i class="fa fa-btn fa-sign-in"></i> Login
              </button>

                <!-- <a class="btn btn-default submit" href="index.html">Log in</a>
                <a class="reset_pass" href="#">Lost your password?</a> -->
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <!-- <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p> -->

                <div class="clearfix"></div>
                <br>

                <div>
                  <h1><i class="fa fa-camera-retro"></i> Museum Collection!</h1>
                  <p>©2016 All Rights Reserved, Museum Collection!.<br> Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>

</body>

@endsection
