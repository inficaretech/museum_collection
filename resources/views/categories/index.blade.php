@extends('layouts.app')
@section('content')

    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Categories
        </h1>

    </div>

    <div class="right_col" role="main">

    <div class="row">
        <div class="col-md-12">
                <table id='datatable' class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>NAME</th>
                        <th>SUB_CATEGORY</th>
                        <th>Feature Image</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                     <?php foreach ($categories as $key => $value) { ?>

                       <tr>
                         <td>{{$key+1}}</td>
                         @if(!empty($value['m_poster_image']))
                             <td class="profile_pic"><img src="http://onlinecollections.anchoragemuseum.org/uploaded_files/{{$value['image']}}" class="image-class" id= "img-class" style="width:60px;"/></td>
                          @else
                           <td class="profile_pic"><img src="{{ asset( './uploads/missing_image.png' ) }}" class="image-class" id= "img-class" style="width:60px;"/></td>
                          @endif

                         <td>{{$value['Classification']}}</td>
                         <td>{{$value['Subclassification']}}</td>
                         <td>
                           <form id="myForm" method="GET" action="categories/posterchange">
                             <input type="hidden" name="id" id="mySelect" value="{{$value['ClassificationID']}}"/>
                             <select class="my-select" name="poster_image" id="my-select1" onchange="this.form.submit()">
                               <option value="">Select Image</option>

                               @if(!empty($value['media'])) {
                                 @for ($i=0; $i < count($value['media']); $i++)
                                   @if(!empty($value['media'][$i]->RenditionNumber))
                                     <option data-img-src="http://onlinecollections.anchoragemuseum.org/uploaded_files/{{ $value['media'][$i]->FileName }}" value="{{$value['media'][$i]->MediaMasterID}}">&nbsp;</option>
                                   @else
                                     <option value="">N/A</option>
                                    @endif
                                    @endfor
                                    @endif
                             </select>
                           </form>
                       </td>


                         <!-- {{ $id =  $value['ClassificationID']}} -->
                     <td class="text-right">
                         <a class="btn btn-xs btn-primary" href="{{ route('categories.show', $id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                     </td>

                       </tr>

                    <?php  } ?>


                </table>
        </div>
    </div>
  </div>

  <style type="text/css">
      .big_table{ overflow: auto;}
  </style>

  </script>
  <script type="text/javascript">
  $(".my-select").chosen({width:"100%"});
  </script>

  <script>
  $(document).ready(function() {
  $('#datatable').DataTable({
          stateSave: true
  });
  });
  </script>

@endsection
