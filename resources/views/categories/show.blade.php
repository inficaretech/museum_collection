@extends('layouts.app')
@section('content')

<div class="right_col" role="main">

<div class="page-header">
        <h1>Category / Show </h1>

    </div>
    <div class="x_panel">
      <div class="x_title">
              <ul class="nav navbar-right panel_toolbox">
              </ul>
              <div class="clearfix"></div>
            </div>
    <div class="row">
        <div class="col-md-6 view_page">

            <form action="#">
              <table class="table table-condensed table-striped">
                      <tr>
                        <td><b>Classification:</b></td>
                        <td>{{$new_categories['Classification']}}</td>
                      </tr>
                      <tr>
                        <td><b>Subclassification:</b></td>
                        <td>{{$new_categories['Subclassification']}}</td>
                      </tr>
                  </table>
              </form>
        </div>
        <div class="col-md-6 view_page">
          <table class="table table-condensed table-striped">
              <tr>
                 @if(!empty($new_categories['image']))
                <td style="width:100%;" class="profile_pic"><img src="http://onlinecollections.anchoragemuseum.org/uploaded_files/{{$new_categories['image']}}" class="image-class" style="width:50%; height:auto; border:#ccc solid 1px; padding:1px;" /></td>
                  @else
                <td class="profile_pic"><img src="{{ asset( './uploads/missing_image.png' ) }}" class="image-class" id= "img-class" style="width:100%;"/></td>
                @endif
            </tr>
          </table>
      </div>
    </div>
    <a class="btn btn-link" href="{{ route('categories.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
</div>
</div>
@endsection
