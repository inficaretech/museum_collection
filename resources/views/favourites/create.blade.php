@extends('layouts.app')
@section('content')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
<div class="right_col" role="main">

    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Favourites / Create </h1>
    </div>


    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('favourites.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('object_id')) has-error @endif">
                       <label for="object_id-field">Object_id</label>
                    <input type="text" id="object_id-field" name="object_id" class="form-control" value="{{ old("object_id") }}"/>
                       @if($errors->has("object_id"))
                        <span class="help-block">{{ $errors->first("object_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('media_master_id')) has-error @endif">
                       <label for="media_master_id-field">Media_master_id</label>
                    <input type="text" id="media_master_id-field" name="media_master_id" class="form-control" value="{{ old("media_master_id") }}"/>
                       @if($errors->has("media_master_id"))
                        <span class="help-block">{{ $errors->first("media_master_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('user_id')) has-error @endif">
                       <label for="user_id-field">User_id</label>
                    <input type="text" id="user_id-field" name="user_id" class="form-control" value="{{ old("user_id") }}"/>
                       @if($errors->has("user_id"))
                        <span class="help-block">{{ $errors->first("user_id") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('favourites.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
  </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
