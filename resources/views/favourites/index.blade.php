@extends('layouts.app')
@section('content')

<div class="right_col" role="main">

    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Favourites
            <a class="btn btn-success pull-right" href="{{ route('favourites.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>

    <div class="row">
        <div class="col-md-12">
            @if($favourites->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>OBJECT_ID</th>
                        <th>MEDIA_MASTER_ID</th>
                        <th>USER_ID</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($favourites as $favourite)
                            <tr>
                                <td>{{$favourite->id}}</td>
                                <td>{{$favourite->object_id}}</td>
                    <td>{{$favourite->media_master_id}}</td>
                    <td>{{$favourite->user_id}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('favourites.show', $favourite->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('favourites.edit', $favourite->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('favourites.destroy', $favourite->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $favourites->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>
</div>
@endsection
