@extends('layouts.app')
@section('content')

<div class="right_col" role="main">

<div class="page-header">
        <h1>Favourites / Show #{{$favourite->id}}</h1>
        <form action="{{ route('favourites.destroy', $favourite->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('favourites.edit', $favourite->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="object_id">OBJECT_ID</label>
                     <p class="form-control-static">{{$favourite->object_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="media_master_id">MEDIA_MASTER_ID</label>
                     <p class="form-control-static">{{$favourite->media_master_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="user_id">USER_ID</label>
                     <p class="form-control-static">{{$favourite->user_id}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('favourites.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>
  </div>

@endsection
