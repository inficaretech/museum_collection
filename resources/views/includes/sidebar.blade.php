<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="/" class="site_title"><i class="fa fa-camera-retro"></i> <span>Museum Collection</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">

                <img src="{{ asset('/uploads/default_avatar.png') }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome <b>Admin</b></span>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li><a href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true"></i> Home <span class="fa fa-chevron"></span></a>
                        <!-- <ul class="nav child_menu">
                            <li><a href="index.html">Dashboard</a></li>
                            <li><a href="index2.html">Dashboard2</a></li>
                            <li><a href="index3.html">Dashboard3</a></li>
                        </ul> -->
                    </li>
                    <li><a href="{{ url('/m_main_categories/') }}"><i class="fa fa-bars" aria-hidden="true"></i>Main Menu</a><!-- Main Menu<span class="fa fa-chevron-down"></span> -->
                            <!-- <li><a href="{{ url('/main_categories/') }}">List</a></li> -->
                           <!--  <li><a href="{{ url('/main_categories/create/') }}">Create</a></li> -->
                            <!-- <li><a href="form_validation.html">Form Validation</a></li>
                            <li><a href="form_wizards.html">Form Wizard</a></li>
                            <li><a href="form_upload.html">Form Upload</a></li>
                            <li><a href="form_buttons.html">Form Buttons</a></li>
                        </ul> -->
                    </li>
                   <li><a href="{{ url('/galleries/') }}"><i class="fa fa-picture-o" aria-hidden="true"></i>Galleries<span class="fa fa-chevron"></span></a>
                        <!-- <ul class="nav child_menu"> -->
                            <!-- <li><a href="{{ url('/galleries/') }}">List</a></li> -->
                             <!-- <li><a href="{{ url('/objects/') }}">Create</a></li>
                             <li><a href="typography.html">Typography</a></li>
                            <li><a href="icons.html">Icons</a></li>
                            <li><a href="glyphicons.html">Glyphicons</a></li>
                            <li><a href="widgets.html">Widgets</a></li>
                            <li><a href="invoice.html">Invoice</a></li>
                            <li><a href="inbox.html">Inbox</a></li>
                            <li><a href="calendar.html">Calendar</a></li> -->
                        <!-- </ul> -->
                    </li>
                     <li><a href="{{ url('/artists/') }}"><i  class="fa fa-user" aria-hidden="true"></i>Artists<span class="fa fa-chevron"></span></a>
                     <li><a href="{{ url('/categories/') }}"><i class="fa fa-user" aria-hidden="true"></i>Categories<span class="fa fa-chevron"></span></a>
                     <!-- <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Cultures<span class="fa fa-chevron"></span></a> -->
                      <!-- <li><a href="{{ url('/server_details/') }}"><i class="fa fa-database" aria-hidden="true"></i>Server Links<span class="fa fa-chevron"></span></a> -->

                        <!-- <ul class="nav child_menu">
                            <li><a href="{{ url('/artists/') }}">List</a></li>
                        </ul> -->
                    </li>
                   <!--   <li><a><i class="fa fa-bar-chart-o"></i>Favorites<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="{{ url('/favorites/') }}">List</a></li>
                         <li><a href="{{ url('/favorites/create') }}">Create</a></li> -->
                            <!-- <li><a href="morisjs.html">Moris JS</a></li>
                            <li><a href="echarts.html">ECharts</a></li>
                            <li><a href="other_charts.html">Other Charts</a></li>
                        </ul>
                    </li>-->
                 <!--      <li><a><i class="fa fa-clone"></i>Object<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="{{ url('/objects/') }}">List</a></li>
                        <li><a href="{{ url('/objects/create') }}">Create</a></li>
                        </ul>
                    </li>-->
                 <!--    <li><a><i class="fa fa-clone"></i>Media<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="{{ url('/media/') }}">List</a></li>
                          <li><a href="{{ url('/media/create') }}">Create</a></li>
                        </ul>
                    </li> -->
                    <li><a href="{{ url('/appheadings/') }}"><i class="fa fa-header" aria-hidden="true"></i>Content Designer</a></span></a>
                       <!--  <ul class="nav child_menu">
                             <li><a href="{{ url('/appheadings/') }}">List</a></li>
                        </ul> -->
                    </li>
                </ul>
            </div>
            <!-- <div class="menu_section">
                <h3>Live On</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="e_commerce.html">E-commerce</a></li>
                            <li><a href="projects.html">Projects</a></li>
                            <li><a href="project_detail.html">Project Detail</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="profile.html">Profile</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="page_403.html">403 Error</a></li>
                            <li><a href="page_404.html">404 Error</a></li>
                            <li><a href="page_500.html">500 Error</a></li>
                            <li><a href="plain_page.html">Plain Page</a></li>
                            <li><a href="login.html">Login Page</a></li>
                            <li><a href="pricing_tables.html">Pricing Tables</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="#level1_1">Level One</a>
                            <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li class="sub_menu"><a href="level2.html">Level Two</a>
                                    </li>
                                    <li><a href="#level2_1">Level Two</a>
                                    </li>
                                    <li><a href="#level2_2">Level Two</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#level1_2">Level One</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
                </ul>
            </div> -->

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <!-- <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a id="menu_tog" data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a href="\logout" data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div> -->
        <!-- /menu footer buttons -->
    </div>
</div>
