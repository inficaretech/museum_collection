<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Museum Collection | </title>

        <!-- jQuery -->
        <script src="{{ asset("js/jquery.min.js") }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset("js/bootstrap.min.js") }}"></script>

        <!-- Bootstrap -->
        <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">

        <script src="{{ asset("js/bootstrap-colorpicker.min.js") }}"></script>
        <!-- Bootstrap -->
        <link href="{{ asset("css/bootstrap-colorpicker.min.css") }}" rel="stylesheet">
        <!-- Custom Theme CSS -->
        <link href="{{{ asset('/css/custome.css') }}}" rel="stylesheet">

        <!-- <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet"> -->

        <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
      <link href="{{ asset("css/dd.css") }}" rel="stylesheet">
      <link href="{{ asset("css/chosen.min.css") }}" rel="stylesheet">
      <link href="{{ asset("css/ImageSelect.css") }}" rel="stylesheet">
      <script src="{{ asset("js/chosen.jquery.min.js") }}"></script>
      <script src="{{ asset("js/ImageSelect.jquery.js") }}"></script>

        <!-- <link rel="stylesheet" href="http://davidstutz.github.io/bootstrap-multiselect/dist/css/bootstrap-multiselect.css" type="text/css"> -->



      <!-- <script type="text/javascript" src="http://davidstutz.github.io/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script> -->



        @stack('stylesheets')

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                @include('includes/sidebar')
                @include('includes/topbar')
                @yield('content')
            </div>
        </div>

        <!-- footer content -->
        <footer>
            <div class="pull-right">
               <a href="https://inficaretech.com">Inficare</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->


        <!-- Custom Theme Scripts -->
        <script src="{{ asset("js/gentelella.min.js") }}"></script>
        <!-- Custom Theme Scripts -->
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>


        @stack('scripts')

    </body>
</html>
