@extends('layouts.app')
@section('content')
@section('css')

  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection


@include('error')

<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="page-header clearfix">
                  <h1>
                      <i class="glyphicon glyphicon-edit"></i> Main Category Update
                      <a class="btn btn-success pull-right" href="{{ route('m_main_categories.index') }}"><i class="glyphicon glyphicon-plus"></i> Index</a>

                      <!-- <h1><i class="glyphicon glyphicon-edit"></i> MainCategories / Edit #{{$main_category->id}}</h1> -->
                  </h1>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <!-- <h2>L<small>ist</small></h2> -->
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" style="display: block;">

            <form action="{{ route('m_main_categories.update', $main_category->id) }}" method="POST" enctype='multipart/form-data' class="form-horizontal form-label-left">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label class= "control-label col-md-3 col-sm-3 col-xs-12" for="title-field">Title</label>
                       <div  class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ is_null(old("title")) ? $main_category->title : old("title") }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                     </div>
                    </div>
              <div class="form-group @if($errors->has('text_color')) has-error @endif">
                     <label class= "control-label col-md-3 col-sm-3 col-xs-12" for="background_color-field">Text Color</label>
                     <div id="cp1" class="col-md-6 col-sm-6 col-xs-12 input-group colorpicker-component">
                  <input type="text" id="text_color-field" name="text_color" class="form-control" value="{{ is_null(old("text_color")) ? $main_category->text_color : old("text_color") }}"/><span class="input-group-addon"><i></i></span>
                     @if($errors->has("text_color"))
                      <span class="help-block">{{ $errors->first("text_color") }}</span>
                     @endif
                   </div>
                  </div>
                  <div class="form-group @if($errors->has('background_color')) has-error @endif">
                         <label class= "control-label col-md-3 col-sm-3 col-xs-12" for="background_color-field">Background Color</label>
                         <div id="cp2" class="col-md-6 col-sm-6 col-xs-12 input-group colorpicker-component">
                      <input type="text" id="background_color-field" name="background_color" class="form-control" value="{{ is_null(old("background_color")) ? $main_category->background_color : old("background_color") }}"/><span class="input-group-addon"><i></i></span>
                         @if($errors->has("background_color"))
                          <span class="help-block">{{ $errors->first("background_color") }}</span>
                         @endif
                      </div>
                    </div>

                    <div class="form-group @if($errors->has('image')) has-error @endif">
                       <label class= "control-label col-md-3 col-sm-3 col-xs-12" for="dimensions-field">Image</label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                       @if($main_category->image_name !="")
                        <img src="{{ asset( '/uploads/main_category').'/'.$main_category->image_name }}" class="image-class" id= "img-class" style="width:60px;"/>
                       @endif
                       <input type="file" id="image-field" name="image" class="form-control" value="{{ old("image") }}"/>
                       @if($errors->has("image"))
                        <span class="help-block">{{ $errors->first("image") }}</span>
                       @endif
                    </div>
                  </div>

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('m_main_categories.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
  </div>
</div>
</div>
</div>

<script src="{{ asset("js/bootstrap-colorpicker.min.js") }}"></script>
<!-- Bootstrap -->
<link href="{{ asset("css/bootstrap-colorpicker.min.css") }}" rel="stylesheet">
  <!-- <script src="{{ asset("js/bootstrap-colorpicker.min.js") }}"></script> -->
  <!-- Bootstrap -->
  <!-- <link href="{{ asset("css/bootstrap-colorpicker.min.css") }}" rel="stylesheet"> -->

  <script>
  $(function() {
    $('#cp1').colorpicker();
  });
  </script>

  <script>
  $(function() {
    $('#cp2').colorpicker();
  });
  </script>

@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
