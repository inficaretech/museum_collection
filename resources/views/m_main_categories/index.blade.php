@extends('layouts.app')
@section('content')

<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="page-header clearfix">
                  <h1>
                      <i class="glyphicon glyphicon-align-justify"></i> Main Menu
                      <!-- Trigger/Open The Modal -->

                      <!-- The Modal -->
                      <div id="myModal" class="modal">

                        <!-- Modal content -->
                        <div class="modal-content">
                          <span class="close">x</span>
                          <div style="width:330px; margin:0 auto; background:#eee; padding:10px;">
                          <ul style="margin:0; padding:0; list-style:none;">
                          	  <li style="background:{{$m_main_categories[0]->background_color}}; height:110px; text-align:center; font-family:arial; color:{{$m_main_categories[0]->text_color}}; width:149px; vertical-align:top; display:inline-block; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; margin-right:3px; margin-bottom:10px;">
                                <div style="width:149px; height:110px; display:table-cell; vertical-align:middle; text-align:center;">{{$m_main_categories[0]->title}}</div>
                              </li>
                              @if(!empty($m_main_categories[0]->image_name))
                              <li style="height:110px; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; text-align:center; font-family:arial; color:#fff;  display:inline-block; vertical-align:top; width:149px; margin-bottom:10px;"><img class="main-cat-img" src="{{ asset( 'uploads/main_category/' )."/".$m_main_categories[0]->image_name }}" alt=""/></li>
                              @else
                              <li style="height:110px; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; text-align:center; font-family:arial; color:#fff;  display:inline-block; vertical-align:top; width:149px; margin-bottom:10px;"><img class="main-cat-img" src="{{ asset( 'uploads/default_main_category/1.jpg' ) }}" alt=""/></li>
                              @endif

                              @if(!empty($m_main_categories[0]->image_name))
                              <li style="height:110px; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; text-align:center; font-family:arial; color:#fff;  display:inline-block; vertical-align:top; width:149px; margin-right:3px; margin-bottom:10px;"><img class="main-cat-img" src="{{ asset( 'uploads/main_category/' )."/".$m_main_categories[1]->image_name }}" alt=""/></li>
                              @else
                              <li style="height:110px; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; text-align:center; font-family:arial; color:#fff;  display:inline-block; vertical-align:top; width:149px; margin-bottom:10px;"><img class="main-cat-img" src="{{ asset( 'uploads/default_main_category/2.jpg' ) }}" alt=""/></li>
                              @endif

                              <li style="background:{{$m_main_categories[1]->background_color}}; height:110px; text-align:center; font-family:arial; color:{{$m_main_categories[1]->text_color}}; width:149px; vertical-align:top; display:inline-block; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; margin-bottom:10px;">
                                <div style="width:149px; height:110px; display:table-cell; vertical-align:middle; text-align:center;">{{$m_main_categories[1]->title}}</div>
                              </li>

                              <li style="background:{{$m_main_categories[2]->background_color}}; height:110px; text-align:center; font-family:arial; color:{{$m_main_categories[2]->text_color}}; width:149px; vertical-align:top; display:inline-block; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; margin-bottom:10px; margin-right:3px;">
                                <div style="width:149px; height:110px; display:table-cell; vertical-align:middle; text-align:center;">{{$m_main_categories[2]->title}}</div>
                              </li>

                              @if(!empty($m_main_categories[0]->image_name))
                              <li style="height:110px; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; text-align:center; font-family:arial; color:#fff;  display:inline-block; vertical-align:top; width:149px; margin-bottom:10px;"><img class="main-cat-img" src="{{ asset( 'uploads/main_category/' )."/".$m_main_categories[2]->image_name }}" alt=""/></li>
                              @else
                              <li style="height:110px; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; text-align:center; font-family:arial; color:#fff;  display:inline-block; vertical-align:top; width:149px; margin-bottom:10px;"><img class="main-cat-img" src="{{ asset( 'uploads/default_main_category/3.jpg' ) }}" alt=""/></li>
                              @endif

                              @if(!empty($m_main_categories[0]->image_name))
                              <li style="height:110px; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; text-align:center; font-family:arial; color:#fff;  display:inline-block; vertical-align:top; width:149px; margin-right:3px; margin-bottom:10px;"><img class="main-cat-img" src="{{ asset( 'uploads/main_category/' )."/".$m_main_categories[3]->image_name }}" alt=""/></li>
                              @else
                              <li style="height:110px; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; text-align:center; font-family:arial; color:#fff;  display:inline-block; vertical-align:top; width:149px; margin-bottom:10px;"><img class="main-cat-img" src="{{ asset( 'uploads/default_main_category/4.jpg' ) }}" alt=""/></li>
                              @endif
                              <li style="background:{{$m_main_categories[3]->background_color}}; height:110px; text-align:center; font-family:arial; color:{{$m_main_categories[3]->text_color}}; width:149px; vertical-align:top; display:inline-block; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; margin-bottom:10px;">
                                <div style="width:149px; height:110px; display:table-cell; vertical-align:middle; text-align:center;">{{$m_main_categories[3]->title}}</div>
                              </li>

                          </ul>
                          </div>
                          </div>

                      </div>
                  </h1>

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                    <button id="myBtn" class="btn btn-small">Preview</button>
                  </p>
                  <div class="clearfix"></div>
                  </div>

                  <div class="x_content" style="display: block;">

            @if($m_main_categories->count())
                <table class="table table-condensed table-striped" id="datatable">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($m_main_categories as $main_category)
                            <tr>
                                <td>{{$main_category->id}}</td>
                                <td>{{$main_category->title}}</td>
                                <td><img src="{{ asset( '/uploads/main_category').'/'.$main_category->image_name }}" class="image-class" id= "img-class" style="width:60px;"/></td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('m_main_categories.show', $main_category->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('m_main_categories.edit', $main_category->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $m_main_categories->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>
  </div>
</div>
</div>
</div>



  <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script>
  $('#datatable').DataTable();
  </script>

  <script>
      // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
  </script>

@endsection
