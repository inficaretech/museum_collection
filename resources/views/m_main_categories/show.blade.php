@extends('layouts.app')
@section('content')
<div class="right_col" role="main">
  <div class="page-title">
<div class="page-header">
        <h1><i class="glyphicon glyphicon-align-justify"></i> MainCategories / Show

         <!--  <form action="{{ route('m_main_categories.destroy', $main_category->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="btn-group pull-right" role="group" aria-label="...">
                  <a class="btn btn-warning btn-group" role="group" href="{{ route('m_main_categories.edit', $main_category->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                  <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
              </div>
          </form> -->
        </h1>

    </div>

    </div>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <form action="#">
                <div class="col-md-4">
                  <div style="width:530px; margin:0 auto; background:#eee; padding:10px;">
                  <ul style="margin:0; padding:0; list-style:none;">
                      <li style="background:{{$main_category->background_color}}; height:210px; text-align:center; font-family:arial; color:{{$main_category->text_color}}; width:249px; vertical-align:top; display:inline-block; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; margin-right:3px; margin-bottom:10px;">
                        <div style="width:249px; height:210px; display:table-cell; vertical-align:middle; font-size:40px; text-align:center;">{{$main_category->title}}</div>
                      </li>
                      <li style="height:210px; box-shadow:1px 3px 2px #a1a1a1 <span class='hexPreview' style='background-color: #a1a1a1'>&nbsp;</span>; text-align:center; font-family:arial; color:#fff;  display:inline-block; vertical-align:top; width:249px; margin-bottom:10px;"><img src="{{ asset( 'uploads/main_category/' )."/".$main_category->image_name }}" style='width:249px; height:210px;' alt=""/></li>
                  </ul>
                  </div>
                </div>
            </form>
             <div class="back-btn">
            <a class="btn btn-link" href="{{ route('m_main_categories.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
          </div>
          </div>
        </div>
    </div>




  </div>







@endsection
