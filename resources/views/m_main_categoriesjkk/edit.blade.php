@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> MMainCategories / Edit #{{$m_main_category->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('m_main_categories.update', $m_main_category->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label for="title-field">Title</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ is_null(old("title")) ? $m_main_category->title : old("title") }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('text_color')) has-error @endif">
                       <label for="text_color-field">Text_color</label>
                    <input type="text" id="text_color-field" name="text_color" class="form-control" value="{{ is_null(old("text_color")) ? $m_main_category->text_color : old("text_color") }}"/>
                       @if($errors->has("text_color"))
                        <span class="help-block">{{ $errors->first("text_color") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('background_color')) has-error @endif">
                       <label for="background_color-field">Background_color</label>
                    <input type="text" id="background_color-field" name="background_color" class="form-control" value="{{ is_null(old("background_color")) ? $m_main_category->background_color : old("background_color") }}"/>
                       @if($errors->has("background_color"))
                        <span class="help-block">{{ $errors->first("background_color") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('image_name')) has-error @endif">
                       <label for="image_name-field">Image_name</label>
                    <input type="text" id="image_name-field" name="image_name" class="form-control" value="{{ is_null(old("image_name")) ? $m_main_category->image_name : old("image_name") }}"/>
                       @if($errors->has("image_name"))
                        <span class="help-block">{{ $errors->first("image_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('image_type')) has-error @endif">
                       <label for="image_type-field">Image_type</label>
                    <input type="text" id="image_type-field" name="image_type" class="form-control" value="{{ is_null(old("image_type")) ? $m_main_category->image_type : old("image_type") }}"/>
                       @if($errors->has("image_type"))
                        <span class="help-block">{{ $errors->first("image_type") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('m_main_categories.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
