@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> MMainCategories
            <a class="btn btn-success pull-right" href="{{ route('m_main_categories.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($m_main_categories->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>TITLE</th>
                        <th>TEXT_COLOR</th>
                        <th>BACKGROUND_COLOR</th>
                        <th>IMAGE_NAME</th>
                        <th>IMAGE_TYPE</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($m_main_categories as $m_main_category)
                            <tr>
                                <td>{{$m_main_category->id}}</td>
                                <td>{{$m_main_category->title}}</td>
                    <td>{{$m_main_category->text_color}}</td>
                    <td>{{$m_main_category->background_color}}</td>
                    <td>{{$m_main_category->image_name}}</td>
                    <td>{{$m_main_category->image_type}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('m_main_categories.show', $m_main_category->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('m_main_categories.edit', $m_main_category->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('m_main_categories.destroy', $m_main_category->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $m_main_categories->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection