@extends('layout')
@section('header')
<div class="page-header">
        <h1>MMainCategories / Show #{{$m_main_category->id}}</h1>
        <form action="{{ route('m_main_categories.destroy', $m_main_category->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('m_main_categories.edit', $m_main_category->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="title">TITLE</label>
                     <p class="form-control-static">{{$m_main_category->title}}</p>
                </div>
                    <div class="form-group">
                     <label for="text_color">TEXT_COLOR</label>
                     <p class="form-control-static">{{$m_main_category->text_color}}</p>
                </div>
                    <div class="form-group">
                     <label for="background_color">BACKGROUND_COLOR</label>
                     <p class="form-control-static">{{$m_main_category->background_color}}</p>
                </div>
                    <div class="form-group">
                     <label for="image_name">IMAGE_NAME</label>
                     <p class="form-control-static">{{$m_main_category->image_name}}</p>
                </div>
                    <div class="form-group">
                     <label for="image_type">IMAGE_TYPE</label>
                     <p class="form-control-static">{{$m_main_category->image_type}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('m_main_categories.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection