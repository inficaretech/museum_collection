@extends('layouts.app')
@section('content')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection

<div class="right_col" role="main">

    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Media / Create </h1>
    </div>

    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('media.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('object_id')) has-error @endif">
                       <label for="object_id-field">Object_id</label>
                    <input type="text" id="object_id-field" name="object_id" class="form-control" value="{{ old("object_id") }}"/>
                       @if($errors->has("object_id"))
                        <span class="help-block">{{ $errors->first("object_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('media_master_id')) has-error @endif">
                       <label for="media_master_id-field">Media_master_id</label>
                    <input type="text" id="media_master_id-field" name="media_master_id" class="form-control" value="{{ old("media_master_id") }}"/>
                       @if($errors->has("media_master_id"))
                        <span class="help-block">{{ $errors->first("media_master_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('display_rend_id')) has-error @endif">
                       <label for="display_rend_id-field">Display_rend_id</label>
                    <input type="text" id="display_rend_id-field" name="display_rend_id" class="form-control" value="{{ old("display_rend_id") }}"/>
                       @if($errors->has("display_rend_id"))
                        <span class="help-block">{{ $errors->first("display_rend_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('public_caption')) has-error @endif">
                       <label for="public_caption-field">Public_caption</label>
                    <input type="text" id="public_caption-field" name="public_caption" class="form-control" value="{{ old("public_caption") }}"/>
                       @if($errors->has("public_caption"))
                        <span class="help-block">{{ $errors->first("public_caption") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('approved_for_web')) has-error @endif">
                       <label for="approved_for_web-field">Approved_for_web</label>
                    <input type="text" id="approved_for_web-field" name="approved_for_web" class="form-control" value="{{ old("approved_for_web") }}"/>
                       @if($errors->has("approved_for_web"))
                        <span class="help-block">{{ $errors->first("approved_for_web") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('rendition_number')) has-error @endif">
                       <label for="rendition_number-field">Rendition_number</label>
                    <input type="text" id="rendition_number-field" name="rendition_number" class="form-control" value="{{ old("rendition_number") }}"/>
                       @if($errors->has("rendition_number"))
                        <span class="help-block">{{ $errors->first("rendition_number") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('sort_number')) has-error @endif">
                       <label for="sort_number-field">Sort_number</label>
                    <input type="text" id="sort_number-field" name="sort_number" class="form-control" value="{{ old("sort_number") }}"/>
                       @if($errors->has("sort_number"))
                        <span class="help-block">{{ $errors->first("sort_number") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('path_id')) has-error @endif">
                       <label for="path_id-field">Path_id</label>
                    <input type="text" id="path_id-field" name="path_id" class="form-control" value="{{ old("path_id") }}"/>
                       @if($errors->has("path_id"))
                        <span class="help-block">{{ $errors->first("path_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('file_name')) has-error @endif">
                       <label for="file_name-field">File_name</label>
                    <input type="text" id="file_name-field" name="file_name" class="form-control" value="{{ old("file_name") }}"/>
                       @if($errors->has("file_name"))
                        <span class="help-block">{{ $errors->first("file_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('format_id')) has-error @endif">
                       <label for="format_id-field">Format_id</label>
                    <input type="text" id="format_id-field" name="format_id" class="form-control" value="{{ old("format_id") }}"/>
                       @if($errors->has("format_id"))
                        <span class="help-block">{{ $errors->first("format_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('path')) has-error @endif">
                       <label for="path-field">Path</label>
                    <input type="text" id="path-field" name="path" class="form-control" value="{{ old("path") }}"/>
                       @if($errors->has("path"))
                        <span class="help-block">{{ $errors->first("path") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('physical_path')) has-error @endif">
                       <label for="physical_path-field">Physical_path</label>
                    <input type="text" id="physical_path-field" name="physical_path" class="form-control" value="{{ old("physical_path") }}"/>
                       @if($errors->has("physical_path"))
                        <span class="help-block">{{ $errors->first("physical_path") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('format')) has-error @endif">
                       <label for="format-field">Format</label>
                    <input type="text" id="format-field" name="format" class="form-control" value="{{ old("format") }}"/>
                       @if($errors->has("format"))
                        <span class="help-block">{{ $errors->first("format") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('media_type_id')) has-error @endif">
                       <label for="media_type_id-field">Media_type_id</label>
                    <input type="text" id="media_type_id-field" name="media_type_id" class="form-control" value="{{ old("media_type_id") }}"/>
                       @if($errors->has("media_type_id"))
                        <span class="help-block">{{ $errors->first("media_type_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('media_type')) has-error @endif">
                       <label for="media_type-field">Media_type</label>
                    <input type="text" id="media_type-field" name="media_type" class="form-control" value="{{ old("media_type") }}"/>
                       @if($errors->has("media_type"))
                        <span class="help-block">{{ $errors->first("media_type") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('media_xref_id')) has-error @endif">
                       <label for="media_xref_id-field">Media_xref_id</label>
                    <input type="text" id="media_xref_id-field" name="media_xref_id" class="form-control" value="{{ old("media_xref_id") }}"/>
                       @if($errors->has("media_xref_id"))
                        <span class="help-block">{{ $errors->first("media_xref_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('table_id')) has-error @endif">
                       <label for="table_id-field">Table_id</label>
                    <input type="text" id="table_id-field" name="table_id" class="form-control" value="{{ old("table_id") }}"/>
                       @if($errors->has("table_id"))
                        <span class="help-block">{{ $errors->first("table_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('primary_display')) has-error @endif">
                       <label for="primary_display-field">Primary_display</label>
                    <input type="text" id="primary_display-field" name="primary_display" class="form-control" value="{{ old("primary_display") }}"/>
                       @if($errors->has("primary_display"))
                        <span class="help-block">{{ $errors->first("primary_display") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('db_identifier')) has-error @endif">
                       <label for="db_identifier-field">Db_identifier</label>
                    <input type="text" id="db_identifier-field" name="db_identifier" class="form-control" value="{{ old("db_identifier") }}"/>
                       @if($errors->has("db_identifier"))
                        <span class="help-block">{{ $errors->first("db_identifier") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('media.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
  </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
