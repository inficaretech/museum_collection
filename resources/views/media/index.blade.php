@extends('layouts.app')
@section('content')

<div class="right_col" role="main">


    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Media
            <a class="btn btn-success pull-right" href="{{ route('media.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>

    <div class="row">
        <div class="col-md-12">
            @if($media->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <!-- <th>OBJECT_ID</th> -->
                        <!-- <th>MEDIA_MASTER_ID</th> -->
                        <!-- <th>DISPLAY_REND_ID</th> -->
                        <th>PUBLIC_CAPTION</th>
                        <th>APPROVED_FOR_WEB</th>
                        <th>RENDITION_NUMBER</th>
                        <!-- <th>SORT_NUMBER</th> -->
                        <!-- <th>PATH_ID</th> -->
                        <!-- <th>FILE_NAME</th> -->
                        <!-- <th>FORMAT_ID</th> -->
                        <!-- <th>PATH</th> -->
                        <!-- <th>PHYSICAL_PATH</th> -->
                        <!-- <th>FORMAT</th> -->
                        <!-- <th>MEDIA_TYPE_ID</th> -->
                        <!-- <th>MEDIA_TYPE</th> -->
                        <!-- <th>MEDIA_XREF_ID</th> -->
                        <!-- <th>TABLE_ID</th> -->
                        <!-- <th>PRIMARY_DISPLAY</th> -->
                        <th>DB_IDENTIFIER</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($media as $key => $medium)
                            <tr>
                                <td>{{$key}}</td>
                                <!-- <td>{{$medium->object_id}}</td> -->
                    <!-- <td>{{$medium->media_master_id}}</td> -->
                    <!-- <td>{{$medium->display_rend_id}}</td> -->
                    <td>{{$medium->public_caption}}</td>
                    <td>{{$medium->approved_for_web}}</td>
                    <td>{{$medium->rendition_number}}</td>
                    <!-- <td>{{$medium->sort_number}}</td> -->
                    <!-- <td>{{$medium->path_id}}</td> -->
                    <!-- <td>{{$medium->file_name}}</td> -->
                    <!-- <td>{{$medium->format_id}}</td> -->
                    <!-- <td>{{$medium->path}}</td> -->
                    <!-- <td>{{$medium->physical_path}}</td> -->
                    <!-- <td>{{$medium->format}}</td> -->
                    <!-- <td>{{$medium->media_type_id}}</td> -->
                    <!-- <td>{{$medium->media_type}}</td> -->
                    <!-- <td>{{$medium->media_xref_id}}</td> -->
                    <!-- <td>{{$medium->table_id}}</td> -->
                    <!-- <td>{{$medium->primary_display}}</td> -->
                    <td>{{$medium->db_identifier}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('media.show', $medium->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('media.edit', $medium->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('media.destroy', $medium->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $media->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>
</div>
@endsection
