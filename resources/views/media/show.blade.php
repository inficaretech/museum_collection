@extends('layouts.app')
@section('content')

<div class="right_col" role="main">

<div class="page-header">
        <h1>Media / Show #{{$medium->id}}</h1>
        <form action="{{ route('media.destroy', $medium->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('media.edit', $medium->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="object_id">OBJECT_ID</label>
                     <p class="form-control-static">{{$medium->object_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="media_master_id">MEDIA_MASTER_ID</label>
                     <p class="form-control-static">{{$medium->media_master_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="display_rend_id">DISPLAY_REND_ID</label>
                     <p class="form-control-static">{{$medium->display_rend_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="public_caption">PUBLIC_CAPTION</label>
                     <p class="form-control-static">{{$medium->public_caption}}</p>
                </div>
                    <div class="form-group">
                     <label for="approved_for_web">APPROVED_FOR_WEB</label>
                     <p class="form-control-static">{{$medium->approved_for_web}}</p>
                </div>
                    <div class="form-group">
                     <label for="rendition_number">RENDITION_NUMBER</label>
                     <p class="form-control-static">{{$medium->rendition_number}}</p>
                </div>
                    <div class="form-group">
                     <label for="sort_number">SORT_NUMBER</label>
                     <p class="form-control-static">{{$medium->sort_number}}</p>
                </div>
                    <div class="form-group">
                     <label for="path_id">PATH_ID</label>
                     <p class="form-control-static">{{$medium->path_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="file_name">FILE_NAME</label>
                     <p class="form-control-static">{{$medium->file_name}}</p>
                </div>
                    <div class="form-group">
                     <label for="format_id">FORMAT_ID</label>
                     <p class="form-control-static">{{$medium->format_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="path">PATH</label>
                     <p class="form-control-static">{{$medium->path}}</p>
                </div>
                    <div class="form-group">
                     <label for="physical_path">PHYSICAL_PATH</label>
                     <p class="form-control-static">{{$medium->physical_path}}</p>
                </div>
                    <div class="form-group">
                     <label for="format">FORMAT</label>
                     <p class="form-control-static">{{$medium->format}}</p>
                </div>
                    <div class="form-group">
                     <label for="media_type_id">MEDIA_TYPE_ID</label>
                     <p class="form-control-static">{{$medium->media_type_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="media_type">MEDIA_TYPE</label>
                     <p class="form-control-static">{{$medium->media_type}}</p>
                </div>
                    <div class="form-group">
                     <label for="media_xref_id">MEDIA_XREF_ID</label>
                     <p class="form-control-static">{{$medium->media_xref_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="table_id">TABLE_ID</label>
                     <p class="form-control-static">{{$medium->table_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="primary_display">PRIMARY_DISPLAY</label>
                     <p class="form-control-static">{{$medium->primary_display}}</p>
                </div>
                    <div class="form-group">
                     <label for="db_identifier">DB_IDENTIFIER</label>
                     <p class="form-control-static">{{$medium->db_identifier}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('media.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>
  </div>

@endsection
