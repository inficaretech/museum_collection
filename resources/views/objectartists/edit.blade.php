@extends('layouts.app')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection

<div class="right_col" role="main">

    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Objectartists / Edit #{{$objectartist->id}}</h1>
    </div>

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('objectartists.update', $objectartist->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('object_id')) has-error @endif">
                       <label for="object_id-field">Object_id</label>
                    <input type="text" id="object_id-field" name="object_id" class="form-control" value="{{ is_null(old("object_id")) ? $objectartist->object_id : old("object_id") }}"/>
                       @if($errors->has("object_id"))
                        <span class="help-block">{{ $errors->first("object_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('artist_id')) has-error @endif">
                       <label for="artist_id-field">Artist_id</label>
                    <input type="text" id="artist_id-field" name="artist_id" class="form-control" value="{{ is_null(old("artist_id")) ? $objectartist->artist_id : old("artist_id") }}"/>
                       @if($errors->has("artist_id"))
                        <span class="help-block">{{ $errors->first("artist_id") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('objectartists.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
  </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
