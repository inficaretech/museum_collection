@extends('layouts.app')
@section('content')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
<div class="right_col" role="main">

    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Galleries / Edit </h1>
    </div>


    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('galleries.update', $object->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <!-- <div class="form-group @if($errors->has('artist_id')) has-error @endif">
                       <label for="artist_id-field">Artist_id</label>
                    <input type="text" id="artist_id-field" name="artist_id" class="form-control" value="{{ is_null(old("artist_id")) ? $object->artist_id : old("artist_id") }}"/>
                       @if($errors->has("artist_id"))
                        <span class="help-block">{{ $errors->first("artist_id") }}</span>
                       @endif
                    </div> -->
                    <div class="form-group @if($errors->has('title')) has-error @endif">
                      <label for="title-field">Title</label>
                      <input type="text" id="title-field" name="title" class="form-control" value="{{ is_null(old("title")) ? $object->title : old("title") }}"/>
                      @if($errors->has("title"))
                      <span class="help-block">{{ $errors->first("title") }}</span>
                      @endif
                    </div>
                    <!-- <div class="form-group @if($errors->has('object_id')) has-error @endif">
                       <label for="object_id-field">Object_id</label>
                    <input type="text" id="object_id-field" name="object_id" class="form-control" value="{{ is_null(old("object_id")) ? $object->object_id : old("object_id") }}"/>
                       @if($errors->has("object_id"))
                        <span class="help-block">{{ $errors->first("object_id") }}</span>
                       @endif
                    </div> -->
                    <!-- <div class="form-group @if($errors->has('object_number')) has-error @endif">
                       <label for="object_number-field">Object_number</label>
                    <input type="text" id="object_number-field" name="object_number" class="form-control" value="{{ is_null(old("object_number")) ? $object->object_number : old("object_number") }}"/>
                       @if($errors->has("object_number"))
                        <span class="help-block">{{ $errors->first("object_number") }}</span>
                       @endif
                    </div> -->
                    <!-- <div class="form-group @if($errors->has('category_id')) has-error @endif">
                       <label for="category_id-field">Category_id</label>
                    <input type="text" id="category_id-field" name="category_id" class="form-control" value="{{ is_null(old("category_id")) ? $object->category_id : old("category_id") }}"/>
                       @if($errors->has("category_id"))
                        <span class="help-block">{{ $errors->first("category_id") }}</span>
                       @endif
                    </div> -->
                    <!-- <div class="form-group @if($errors->has('sub_class_id')) has-error @endif">
                       <label for="sub_class_id-field">Sub_class_id</label>
                    <input type="text" id="sub_class_id-field" name="sub_class_id" class="form-control" value="{{ is_null(old("sub_class_id")) ? $object->sub_class_id : old("sub_class_id") }}"/>
                       @if($errors->has("sub_class_id"))
                        <span class="help-block">{{ $errors->first("sub_class_id") }}</span>
                       @endif
                    </div> -->
                    <!-- <div class="form-group @if($errors->has('date_degin')) has-error @endif">
                       <label for="date_degin-field">Date_degin</label>
                    <input type="text" id="date_degin-field" name="date_degin" class="form-control" value="{{ is_null(old("date_degin")) ? $object->date_degin : old("date_degin") }}"/>
                       @if($errors->has("date_degin"))
                        <span class="help-block">{{ $errors->first("date_degin") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('date_end')) has-error @endif">
                       <label for="date_end-field">Date_end</label>
                    <input type="text" id="date_end-field" name="date_end" class="form-control" value="{{ is_null(old("date_end")) ? $object->date_end : old("date_end") }}"/>
                       @if($errors->has("date_end"))
                        <span class="help-block">{{ $errors->first("date_end") }}</span>
                       @endif
                    </div> -->
                    <div class="form-group @if($errors->has('object_name')) has-error @endif">
                       <label for="object_name-field">Object_name</label>
                    <input type="text" id="object_name-field" name="object_name" class="form-control" value="{{ is_null(old("object_name")) ? $object->object_name : old("object_name") }}"/>
                       @if($errors->has("object_name"))
                        <span class="help-block">{{ $errors->first("object_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('medium')) has-error @endif">
                       <label for="medium-field">Medium</label>
                    <input type="text" id="medium-field" name="medium" class="form-control" value="{{ is_null(old("medium")) ? $object->medium : old("medium") }}"/>
                       @if($errors->has("medium"))
                        <span class="help-block">{{ $errors->first("medium") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('dimensions')) has-error @endif">
                       <label for="dimensions-field">Dimensions</label>
                    <textarea class="form-control" id="dimensions-field" rows="3" name="dimensions">{{ is_null(old("dimensions")) ? $object->dimensions : old("dimensions") }}</textarea>
                       @if($errors->has("dimensions"))
                        <span class="help-block">{{ $errors->first("dimensions") }}</span>
                       @endif
                    </div>
                    <!-- <div class="form-group @if($errors->has('signed')) has-error @endif">
                       <label for="signed-field">Signed</label>
                    <input type="text" id="signed-field" name="signed" class="form-control" value="{{ is_null(old("signed")) ? $object->signed : old("signed") }}"/>
                       @if($errors->has("signed"))
                        <span class="help-block">{{ $errors->first("signed") }}</span>
                       @endif
                    </div> -->
                    <div class="form-group @if($errors->has('inscribed')) has-error @endif">
                       <label for="inscribed-field">Inscribed</label>
                    <textarea class="form-control" id="inscribed-field" rows="3" name="inscribed">{{ is_null(old("inscribed")) ? $object->inscribed : old("inscribed") }}</textarea>
                       @if($errors->has("inscribed"))
                        <span class="help-block">{{ $errors->first("inscribed") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('credit_line')) has-error @endif">
                       <label for="credit_line-field">Credit_line</label>
                    <textarea class="form-control" id="credit_line-field" rows="3" name="credit_line">{{ is_null(old("credit_line")) ? $object->credit_line : old("credit_line") }}</textarea>
                       @if($errors->has("credit_line"))
                        <span class="help-block">{{ $errors->first("credit_line") }}</span>
                       @endif
                    </div>
                    <!-- <div class="form-group @if($errors->has('chat')) has-error @endif">
                       <label for="chat-field">Chat</label>
                    <textarea class="form-control" id="chat-field" rows="3" name="chat">{{ is_null(old("chat")) ? $object->chat : old("chat") }}</textarea>
                       @if($errors->has("chat"))
                        <span class="help-block">{{ $errors->first("chat") }}</span>
                       @endif
                    </div> -->
                    <!-- <div class="form-group @if($errors->has('paper_file_ref')) has-error @endif">
                       <label for="paper_file_ref-field">Paper_file_ref</label>
                    <textarea class="form-control" id="paper_file_ref-field" rows="3" name="paper_file_ref">{{ is_null(old("paper_file_ref")) ? $object->paper_file_ref : old("paper_file_ref") }}</textarea>
                       @if($errors->has("paper_file_ref"))
                        <span class="help-block">{{ $errors->first("paper_file_ref") }}</span>
                       @endif
                    </div> -->
                    <!-- <div class="form-group @if($errors->has('exhibitions')) has-error @endif">
                       <label for="exhibitions-field">Exhibitions</label>
                    <textarea class="form-control" id="exhibitions-field" rows="3" name="exhibitions">{{ is_null(old("exhibitions")) ? $object->exhibitions : old("exhibitions") }}</textarea>
                       @if($errors->has("exhibitions"))
                        <span class="help-block">{{ $errors->first("exhibitions") }}</span>
                       @endif
                    </div> -->
                    <div class="form-group @if($errors->has('provenance')) has-error @endif">
                       <label for="provenance-field">Provenance</label>
                    <textarea class="form-control" id="provenance-field" rows="3" name="provenance">{{ is_null(old("provenance")) ? $object->provenance : old("provenance") }}</textarea>
                       @if($errors->has("provenance"))
                        <span class="help-block">{{ $errors->first("provenance") }}</span>
                       @endif
                    </div>
                    <!-- <div class="form-group @if($errors->has('pub_references')) has-error @endif">
                       <label for="pub_references-field">Pub_references</label>
                    <textarea class="form-control" id="pub_references-field" rows="3" name="pub_references">{{ is_null(old("pub_references")) ? $object->pub_references : old("pub_references") }}</textarea>
                       @if($errors->has("pub_references"))
                        <span class="help-block">{{ $errors->first("pub_references") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('curatorial_remarks')) has-error @endif">
                       <label for="curatorial_remarks-field">Curatorial_remarks</label>
                    <input type="text" id="curatorial_remarks-field" name="curatorial_remarks" class="form-control" value="{{ is_null(old("curatorial_remarks")) ? $object->curatorial_remarks : old("curatorial_remarks") }}"/>
                       @if($errors->has("curatorial_remarks"))
                        <span class="help-block">{{ $errors->first("curatorial_remarks") }}</span>
                       @endif
                    </div> -->
                    <!-- <div class="form-group @if($errors->has('related_works')) has-error @endif">
                       <label for="related_works-field">Related_works</label>
                    <input type="text" id="related_works-field" name="related_works" class="form-control" value="{{ is_null(old("related_works")) ? $object->related_works : old("related_works") }}"/>
                       @if($errors->has("related_works"))
                        <span class="help-block">{{ $errors->first("related_works") }}</span>
                       @endif
                    </div> -->
                    <!-- <div class="form-group @if($errors->has('public_access')) has-error @endif">
                       <label for="public_access-field">Public_access</label>
                    <input type="text" id="public_access-field" name="public_access" class="form-control" value="{{ is_null(old("public_access")) ? $object->public_access : old("public_access") }}"/>
                       @if($errors->has("public_access"))
                        <span class="help-block">{{ $errors->first("public_access") }}</span>
                       @endif
                    </div> -->
                    <!-- <div class="form-group @if($errors->has('curator_approved')) has-error @endif">
                       <label for="curator_approved-field">Curator_approved</label>
                    <input type="text" id="curator_approved-field" name="curator_approved" class="form-control" value="{{ is_null(old("curator_approved")) ? $object->curator_approved : old("curator_approved") }}"/>
                       @if($errors->has("curator_approved"))
                        <span class="help-block">{{ $errors->first("curator_approved") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('on_view')) has-error @endif">
                       <label for="on_view-field">On_view</label>
                    <input type="text" id="on_view-field" name="on_view" class="form-control" value="{{ is_null(old("on_view")) ? $object->on_view : old("on_view") }}"/>
                       @if($errors->has("on_view"))
                        <span class="help-block">{{ $errors->first("on_view") }}</span>
                       @endif
                    </div> -->
                    <!-- <div class="form-group @if($errors->has('entered_date')) has-error @endif">
                       <label for="entered_date-field">Entered_date</label>
                    <input type="text" id="entered_date-field" name="entered_date" class="form-control" value="{{ is_null(old("entered_date")) ? $object->entered_date : old("entered_date") }}"/>
                       @if($errors->has("entered_date"))
                        <span class="help-block">{{ $errors->first("entered_date") }}</span>
                       @endif
                    </div> -->
                    <div class="form-group @if($errors->has('culture')) has-error @endif">
                       <label for="culture-field">Culture</label>
                    <input type="text" id="culture-field" name="culture" class="form-control" value="{{ is_null(old("culture")) ? $object->culture : old("culture") }}"/>
                       @if($errors->has("culture"))
                        <span class="help-block">{{ $errors->first("culture") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('bibliography')) has-error @endif">
                       <label for="bibliography-field">Bibliography</label>
                    <textarea class="form-control" id="bibliography-field" rows="3" name="bibliography">{{ is_null(old("bibliography")) ? $object->bibliography : old("bibliography") }}</textarea>
                       @if($errors->has("bibliography"))
                        <span class="help-block">{{ $errors->first("bibliography") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('dated')) has-error @endif">
                       <label for="dated-field">Dated</label>
                    <input type="text" id="dated-field" name="dated" class="form-control" value="{{ is_null(old("dated")) ? $object->dated : old("dated") }}"/>
                       @if($errors->has("dated"))
                        <span class="help-block">{{ $errors->first("dated") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('galleries.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
  </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
