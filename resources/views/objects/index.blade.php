@extends('layouts.app')
@section('content')

<div class="right_col" role="main">

  <div class="page-title">
    <div class="page-header clearfix">
        <h1>
            <i class="fa fa-picture-o"></i> Galleries
        </h1>

    </div>
  </div>

    <div class="row">
        <div class="col-md-12 big_table">
          <div class="x_panel">
            <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                <table id='datatable' class="table table-condensed table-striped">
                    <thead>
                        <tr>
                        <th>S.no</th>
                        <th></th>
                        <th>TITLE</th>
                        <th>OBJECT NAME</th>
                        <th>ARTIST</th>
                        <th>CATEGORY</th>
                        <th>CULTURE</th>
                        <th>CREDIT LINE</th>
                        <th>DATED</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach($new_object as $key => $object) { ?>
                             <tr>
                                <td>{{$key+1}}</td>
                                @if(!empty($object['image_url']))
                                <td class="profile_pic"><img src={{$object['image_url']}} class="image-class" id= "img-class" style="width:60px;"/></td>
                                @else
                                <td class="profile_pic"><img src="{{ asset( './uploads/missing_image.png' ) }}" class="image-class" id= "img-class" style="width:60px;"/></td>
                                @endif
                                <td>{{$object['Title']}}</td>
                                <td>{{$object['ObjectName']}}</td>
                                <td>{{$object['artist']}}</td>
                                <td>{{$object['category']}}</td>
                                <td>{{$object['Culture']}}</td>
                                <td>{{$object['CreditLine']}}</td>
                                <td>{{$object['Dated']}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('galleries.show', $object['ObjectID']) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
              </div>
        </div>
    </div>
  </div>
<style type="text/css">
    .big_table{ overflow: auto;}
</style>
  <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script>
$('#datatable').DataTable();
</script>

@endsection
