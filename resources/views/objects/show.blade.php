@extends('layouts.app')
@section('content')

<div class="right_col" role="main">

<div class="page-header">
        <h1>Gallery / Show </h1>
            <div class="btn-group pull-right" role="group" aria-label="...">
            </div>
    </div>

    <div class="row">
      <div class="x_panel">
        <div class="x_title">
                <ul class="nav navbar-right panel_toolbox">
                </ul>
                <div class="clearfix"></div>
              </div>
        <div class="col-md-6 view_page">

            <form action="#">
              <table class="table table-condensed table-striped">
                      <tr>
                        <td><b>Title:</b></td>
                         <td><?php echo $object['Title']; ?></td>
                      </tr>
                      <tr>
                        <td><b>Artist:</b></td>
                        <td><?php echo $object['artist']; ?></td>
                      </tr>
                      <tr>
                        <td><b>Category:</b></td>
                        <td><?php echo $object['category']; ?></td>
                      </tr>
                      <tr>
                          <td><b>Culture:</b></td>
                           <td><?php echo $object['Culture']; ?></td>
                      </tr>
                      <tr>
                          <td><b>Object Number:</b></td>
                          <td><?php echo $object['ObjectNumber']; ?></td>
                      </tr>
                      <tr>
                          <td><b>Object Name:</b></td>
                          <td><?php echo $object['ObjectName']; ?></td>
                      </tr>
                      <tr>
                          <td><b>Medium:</b></td>
                          <td><?php echo $object['Medium']; ?></td>
                      </tr>
                      <tr>
                          <td><b>Dimensions:</b></td>
                          <td><?php echo $object['Dimensions']; ?></td>
                      </tr>
                  <tr>
                      <td><b>Inscribed:</b></td>
                       <td><?php echo $object['inscribed']; ?></td>
                  </tr>
                  <tr>
                      <td><b>Credit:</b></td>
                       <td><?php echo $object['CreditLine']; ?></td>
                  </tr>
                  <tr>
                      <td><b>Provenance:</b></td>
                       <td><?php echo $object['Provenance']; ?></td>
                  </tr>
                  <tr>
                      <td><b>Public Access:</b></td>
                       <td><?php echo $object['PublicAccess']; ?></td>
                  </tr>

                  <tr>
                      <td><b>Bibliography:</b></td>
                       <td><?php echo $object['Bibliography']; ?></td>
                  </tr>
                  <tr>
                      <td><b>Date:</b></td>
                       <td><?php echo $object['Dated']; ?></td>
                  </tr>

                  </table>

              </form>
              <a class="btn btn-link" href="{{ route('galleries.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
        </div>
        <div class="col-md-6 view_page">
          <table class="table table-condensed table-striped">
              <tr>
              <td style="width:100%;" class="profile_pic"><img src=<?php echo $object['image_url']; ?> class="image-class" style="width:100%; height:auto; border:#ccc solid 1px; padding:1px;" /></td>
            </tr>
          </table>
      </div>
    </div>
    </div>
  </div>

@endsection
