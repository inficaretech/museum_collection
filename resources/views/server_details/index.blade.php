@extends('layouts.app')
@section('content')

<div class="right_col" role="main">
    <div class="page-header clearfix">
        <h1>
            <i class="fa fa-database"></i> Servers
            <!-- <a class="btn btn-success pull-right" href="{{ route('server_details.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a> -->
        </h1>

    </div>

    <div class="row">
        <div class="col-md-6">
           <table>
           <tr>
           <td>
           <button class="btn btn-success active">#Server1(anchorage)</button>
           
           </td>

           <td>
          <button class="btn btn-info">#Server2</button>
           </td>

           <td>
          <button class="btn btn-info">#Server3</button>
           </td>
           </tr>
           </table>

        </div>
    </div>
</div>
@endsection